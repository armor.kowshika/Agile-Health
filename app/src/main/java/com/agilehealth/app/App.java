package com.agilehealth.app;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.agilehealth.session.CommonTitleDao;
import com.agilehealth.session.DaoMaster;
import com.agilehealth.session.DaoSession;
import com.agilehealth.session.ProfileDao;
import com.agilehealth.session.QuestionsDao;
import com.agilehealth.util.toast.ToastMaker;

import io.realm.Realm;
import io.realm.RealmConfiguration;


/**
 * Administrator on 10-May-17.
 */

public class App extends Application {

    public static App mInstance;
    DaoMaster.DevOpenHelper helper;
    DaoMaster daoMaster;
    SQLiteDatabase db;
    DaoSession daoSession;
    public ProfileDao profileDao;
    public CommonTitleDao commonTitleDao;
    public QuestionsDao questionsDao;


    public static App getInstance() {
        return mInstance;
    }

    public void offline()
    {
        ToastMaker.makeToast(getInstance().getApplicationContext(), "Please check your Internet connection");
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        mInstance = this;
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
//        DaggerAppComponent.builder().appModule(new AppModule(this)).build().inject(this);

        // do this once, for example in your Application class
        helper = new DaoMaster.DevOpenHelper(this, "notes-db", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
// do this in your activities/fragments to get hold of a DAO
        profileDao = daoSession.getProfileDao();
        questionsDao=daoSession.getQuestionsDao();
        commonTitleDao=daoSession.getCommonTitleDao();
    }

    public void clearSession() {
        daoSession = daoMaster.newSession();
        profileDao = daoSession.getProfileDao();
        questionsDao=daoSession.getQuestionsDao();
        commonTitleDao=daoSession.getCommonTitleDao();
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }
}
