package com.agilehealth.injection.module;

import com.agilehealth.ui.QuestionsAnswers.presenter.QuestionsAnswersImpl;
import com.agilehealth.ui.QuestionsAnswers.presenter.QuestionsAnswersPresenter;
import com.agilehealth.ui.dashboard_fragment.presenter.DashboardImpl;
import com.agilehealth.ui.dashboard_fragment.presenter.DashboardPresenter;
import com.agilehealth.ui.forgotpassword.presenter.ForgotImpl;
import com.agilehealth.ui.forgotpassword.presenter.ForgotPresenter;
import com.agilehealth.ui.login.presenter.LoginImpl;
import com.agilehealth.ui.login.presenter.LoginPresenter;
import com.agilehealth.ui.paswordupdate.presenter.PasswordImpl;
import com.agilehealth.ui.paswordupdate.presenter.PasswordPresenter;
import com.agilehealth.ui.profilefragment.presenter.ProfileImpl;
import com.agilehealth.ui.profilefragment.presenter.ProfilePresenter;
import com.agilehealth.ui.register.presenter.RegisterImpl;
import com.agilehealth.ui.register.presenter.RegisterPresenter;
import com.agilehealth.ui.main.presenter.MainImpl;
import com.agilehealth.ui.main.presenter.MainPresenter;
import com.agilehealth.ui.splash.presenter.SplashImpl;
import com.agilehealth.ui.splash.presenter.SplashPresenter;
import com.agilehealth.ui.supportfragment.presenter.SupportImpl;
import com.agilehealth.ui.supportfragment.presenter.SupportPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Administrator on 10-May-17.
 */
@Module
public class PresenterModule {

    @Provides
    RegisterPresenter providesRegisterPresenter() {
        return new RegisterImpl();
    }

    @Provides
    LoginPresenter providesLoginPresenter() {
        return new LoginImpl();
    }

    @Provides
    ForgotPresenter providesForgotPresenter() {
        return new ForgotImpl();
    }

    @Provides
    SplashPresenter providesSplashPresenter() {
        return new SplashImpl();
    }

    @Provides
    SupportPresenter providesSupportPresenter() {
        return new SupportImpl();
    }

    @Provides
    MainPresenter providesMainPresenter() {
        return new MainImpl();
    }

    @Provides
    ProfilePresenter providesProfilePresenter()
    {
        return  new ProfileImpl();
    }

    @Provides
    PasswordPresenter providesPasswordPresenter()
    {
        return new PasswordImpl();

    }
    @Provides
    QuestionsAnswersPresenter providesQuestionsAnswersPresenter()
    {
        return new QuestionsAnswersImpl();
    }
    @Provides
    DashboardPresenter providesDashboardPresenter()
    {
        return new DashboardImpl();
    }

}

