package com.agilehealth.injection.module;

import android.content.Context;


import com.agilehealth.app.App;

import dagger.Module;
import dagger.Provides;

/**
 * Administrator on 04-Apr-17.
 */

@Module
public class AppModule {
    private final App application;

    public AppModule(App application) {
        this.application = application;
    }

    @Provides
    App providesApplication() {
        return application;
    }

    @Provides
    Context providesContext() {
        return application;
    }
}
