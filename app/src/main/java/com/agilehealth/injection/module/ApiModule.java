package com.agilehealth.injection.module;


import android.util.Log;

import com.agilehealth.BuildConfig;

import com.agilehealth.data.Config;
import com.agilehealth.data.api.ApiService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Administrator on 04-Apr-17.
 */

@Module(includes = PersistentModule.class)
public class ApiModule {

    public static final String DEFAULT = "non-auth";
    public static final String AUTH = "auth";



    private static final String REQUEST_TAG = "Request";
    private static final String RESPONSE_TAG = "Response";

    public ApiModule() {
    }

    @Named(DEFAULT)
    @Provides
    LoggingInterceptor providesLoggingInterceptor() {
        return new LoggingInterceptor.Builder()
                .loggable(BuildConfig.DEBUG)
                .setLevel(Level.BASIC)
                .log(Log.DEBUG)
                .request(REQUEST_TAG)
                .response(RESPONSE_TAG).build();
    }

    @Named(AUTH)
    @Provides
    LoggingInterceptor providesAuthLoggingInterceptor()
    {
        return new LoggingInterceptor.Builder()
                .loggable(BuildConfig.DEBUG)
                .setLevel(Level.BASIC)
                .addHeader("auth-id", "")
                .addHeader("authorization", "")
                .log(Log.DEBUG)
                .request(REQUEST_TAG)
                .response(RESPONSE_TAG).build();
    }

    @Named(DEFAULT)
    @Provides
    OkHttpClient providesOkHttpClient(@Named(DEFAULT) LoggingInterceptor interceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true).build();
    }

    @Named(AUTH)
    @Provides
    OkHttpClient providesAuthOkHttpClient(@Named(AUTH) LoggingInterceptor interceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)

                .readTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true).build();
    }

    @Provides
    @Singleton
    public Converter.Factory provideGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .serializeNulls()
                .create();
    }


    @Named(DEFAULT)
    @Provides
    Retrofit provideRetrofit(@Named(DEFAULT) OkHttpClient httpClient, Converter.Factory factory) {
        return new Retrofit.Builder()
                .baseUrl(com.agilehealth.data.Config.BASE_URL)
                .addConverterFactory(factory)
                .client(httpClient).build();
    }

    @Named(AUTH)
    @Provides
    Retrofit provideAuthRetrofit(@Named(AUTH) OkHttpClient httpClient, Converter.Factory factory) {
        return new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(factory)
                .client(httpClient).build();
    }

    @Named(DEFAULT)
    @Provides
    ApiService providesApiService(@Named(DEFAULT) Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }


    @Named(AUTH)
    @Provides
    ApiService providesAuthApiService(@Named(AUTH) Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }
}
