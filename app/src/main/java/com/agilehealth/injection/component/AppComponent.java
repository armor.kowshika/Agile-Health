package com.agilehealth.injection.component;


import com.agilehealth.app.App;
import com.agilehealth.injection.module.ApiModule;
import com.agilehealth.injection.module.AppModule;
import com.agilehealth.injection.module.CommonModule;
import com.agilehealth.injection.module.PersistentModule;
import com.agilehealth.injection.module.PresenterModule;
import com.agilehealth.ui.QuestionsAnswers.QuestionsAnswersActivity;
import com.agilehealth.ui.QuestionsAnswers.presenter.QuestionsAnswersImpl;
import com.agilehealth.ui.dashboard_fragment.DashboardFragment;
import com.agilehealth.ui.dashboard_fragment.presenter.DashboardImpl;
import com.agilehealth.ui.forgotpassword.ForgotAcitivity;
import com.agilehealth.ui.forgotpassword.presenter.ForgotImpl;
import com.agilehealth.ui.login.LoginActivity;
import com.agilehealth.ui.login.presenter.LoginImpl;
import com.agilehealth.ui.paswordupdate.ChangePasswordActivity;
import com.agilehealth.ui.paswordupdate.presenter.PasswordImpl;
import com.agilehealth.ui.profilefragment.ProfileFragment;
import com.agilehealth.ui.profilefragment.presenter.ProfileImpl;
import com.agilehealth.ui.register.RegisterActivity;
import com.agilehealth.ui.register.presenter.RegisterImpl;
import com.agilehealth.ui.splash.SpalshActivity;
import com.agilehealth.ui.main.MainActivity;
import com.agilehealth.ui.main.presenter.MainImpl;
import com.agilehealth.ui.splash.presenter.SplashImpl;
import com.agilehealth.ui.supportfragment.SupportFragement;
import com.agilehealth.ui.supportfragment.presenter.SupportImpl;

import javax.inject.Singleton;

import dagger.Component;

/*import com.aseema.home_test_reminder.HomeTestReminderActivity;*/
//import com.aseema.ui.home_test_reminder.presenter.HomeTestReminderImpl;

/**
 * Administrator on 10-May-17.
 */

@Singleton
@Component(modules = {AppModule.class, ApiModule.class, PersistentModule.class, PresenterModule.class, CommonModule.class})
public interface AppComponent {

    void inject(App app);

    void inject(RegisterActivity registerActivity);

    void inject(RegisterImpl register);

    void inject(LoginActivity loginActivity);

    void inject(LoginImpl login);

    void inject(MainActivity mainActivity);

    void inject(SpalshActivity spalshActivity);


    void inject(ForgotAcitivity forgotAcitivity);

    void inject(ForgotImpl forgot);

    void inject(SplashImpl splash);

    void inject(MainImpl main);

    void inject(SupportFragement supportFragement);

    void inject(SupportImpl support);

    void inject(ProfileFragment profileFragment);

    void inject(ProfileImpl profile);

    void inject(PasswordImpl password);

    void inject(ChangePasswordActivity changePasswordActivity);

    void inject(QuestionsAnswersActivity questionsAnswersActivity);

    void inject(QuestionsAnswersImpl questionsAnswers);

    void inject(DashboardFragment dashboardFragment);

    void inject(DashboardImpl dashboard);


}
