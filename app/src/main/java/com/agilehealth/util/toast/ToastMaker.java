package com.agilehealth.util.toast;

import android.content.Context;
import android.widget.Toast;

import com.agilehealth.ui.login.model.Model;

import retrofit2.Callback;

/** MOHAN on 23-May-17. */
public class ToastMaker {

    public static void makeToast(Context context, int message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    public static void makeToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
