package com.agilehealth.util.toast;

/** MOHAN on 23-May-17. */
public interface ToastInterface {
    void makeToast(int message);

    void makeToast(String message);
}
