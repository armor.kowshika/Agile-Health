package com.agilehealth.util.web;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/** MOHAN on 31-May-17. */
public class WebUtils {

    public static MultipartBody.Part getImagePart(String key, File file) {
        try {
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
            return MultipartBody.Part.createFormData(key, file.getName(), requestFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static RequestBody createRequest(String value) {
        try {
            return RequestBody.create(MediaType.parse("text/plain"), value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
