package com.agilehealth.util.web;

import com.google.gson.annotations.Expose;

/**
 * Administrator on 1/17/2017.
 */

public class APIError {
    @Expose
    private String status = "";
    @Expose
    private String message;

    public APIError() {
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
