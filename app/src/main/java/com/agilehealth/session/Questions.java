package com.agilehealth.session;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by ck-nivetha on 16/9/17.
 */@Entity
public class Questions {

    @Id
    public Long id;

    public Long titleId;

    public String questionContent;

    public  String answers;

    @Generated(hash = 721857276)
    public Questions(Long id, Long titleId, String questionContent,
            String answers) {
        this.id = id;
        this.titleId = titleId;
        this.questionContent = questionContent;
        this.answers = answers;
    }

    @Generated(hash = 1000657236)
    public Questions() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTitleId() {
        return this.titleId;
    }

    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }

    public String getQuestionContent() {
        return this.questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getAnswers() {
        return this.answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }


}
