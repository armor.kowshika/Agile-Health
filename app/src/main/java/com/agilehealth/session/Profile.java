package com.agilehealth.session;

import com.google.gson.annotations.Expose;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created on 6/15/2017.
 */
@Entity
public class Profile
{
    @Id
    @Expose
    public Long id;
    @Expose
    public  String firstName;
    @Expose
    public  String lastName;
    @Expose
    public  String email;
    @Expose
    public  String mobile;
    @Expose
    public  String password;
    @Expose
    public  String authToken;
    @Expose
    public  String tempToken;
    @Expose
    public  String profileStatus;
    @Expose
    public  String deviceType;
    @Expose
    public  String deviceToken;
    @Expose
    public  String environment;
    @Expose
    public  String created_at;
    @Expose
    public  String updated_at;
    @Expose
    public String profilePicture;
    @Expose
    public String isLogin;

    public String title;

    public String company;

    public String agileCertification;

    @Generated(hash = 1554026083)
    public Profile(Long id, String firstName, String lastName, String email,
            String mobile, String password, String authToken, String tempToken,
            String profileStatus, String deviceType, String deviceToken,
            String environment, String created_at, String updated_at,
            String profilePicture, String isLogin, String title, String company,
            String agileCertification) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.mobile = mobile;
        this.password = password;
        this.authToken = authToken;
        this.tempToken = tempToken;
        this.profileStatus = profileStatus;
        this.deviceType = deviceType;
        this.deviceToken = deviceToken;
        this.environment = environment;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.profilePicture = profilePicture;
        this.isLogin = isLogin;
        this.title = title;
        this.company = company;
        this.agileCertification = agileCertification;
    }

    @Generated(hash = 782787822)
    public Profile() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthToken() {
        return this.authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getTempToken() {
        return this.tempToken;
    }

    public void setTempToken(String tempToken) {
        this.tempToken = tempToken;
    }

    public String getProfileStatus() {
        return this.profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getDeviceType() {
        return this.deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return this.deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getEnvironment() {
        return this.environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getCreated_at() {
        return this.created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return this.updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getProfilePicture() {
        return this.profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getIsLogin() {
        return this.isLogin;
    }

    public void setIsLogin(String isLogin) {
        this.isLogin = isLogin;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAgileCertification() {
        return this.agileCertification;
    }

    public void setAgileCertification(String agileCertification)
    {
        this.agileCertification = agileCertification;
    }




}
