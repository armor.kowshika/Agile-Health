package com.agilehealth.data.api;


import com.agilehealth.R;

public class ErrorUtils {

    public static final int BLOCKED = 422;
    public static final int INVALID_LOGIN = 423;
    public static final int INTERNAL_SERVER_ERROR = 500;
    public static final int BAD_REQUEST = 400;
    public static final int UNAUTHORIZE = 401;
    public static final int FORBIDDEN = 403;
    public static final int URL_NOT_FOUND = 404;
    public static final int RIDE_NOTFOUND = 421;
    public static final int APP_UPDATE = 420;
    public static final int INVALID = 434;
    public static final int OTP_EXPIRED = 435;

    //    public static APIError parseError(Response<?> response) {
    //        Converter<ResponseBody, APIError> converter = RetrofitSingleton.retrofit.responseBodyConverter(APIError.class, new Annotation[0]);
    //        APIError error;
    //        try {
    //            error = converter.convert(response.errorBody());
    //        } catch (IOException e) {
    //            return new APIError();
    //        }
    //        return error;
    //    }

    public static int getErrorMsg(int code) {
        switch (code) {
            /*case INVALID_LOGIN:
                return R.string.invalid_credentials;
            case INTERNAL_SERVER_ERROR:
                return R.string.server_error;
            case BAD_REQUEST:
                return R.string.fields_missing;
            case UNAUTHORIZE:
                return R.string.unauthorized;
            case FORBIDDEN:
                return R.string.forbidden;
            case URL_NOT_FOUND:
                return R.string.url_not_found;
            case RIDE_NOTFOUND:
                return R.string.ride_not_found;
            case APP_UPDATE:
                return R.string.app_update;
            case INVALID:
                return R.string.invalid_otp;
            case OTP_EXPIRED:
                return R.string.otp_expired;*/
            default:
                return 1;

        }
    }
}
