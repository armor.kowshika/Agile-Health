package com.agilehealth.data.api;

import com.google.gson.annotations.Expose;

/** Administrator on 1/17/2017. */
public class APIError {
    @Expose private String error = "";

    public APIError() {}

    public String getError() {
        return error;
    }
}
