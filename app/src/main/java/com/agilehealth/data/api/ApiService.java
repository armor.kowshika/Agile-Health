package com.agilehealth.data.api;


import com.agilehealth.ui.dashboard_fragment.model.CommentLikeResponse;
import com.agilehealth.ui.dashboard_fragment.model.CommentLikeUpdate;
import com.agilehealth.ui.dashboard_fragment.model.MostPopular;
import com.agilehealth.ui.dashboard_fragment.model.MostPopularResponse;
import com.agilehealth.ui.dashboard_fragment.model.PostRequest;
import com.agilehealth.ui.dashboard_fragment.model.PostResponse;
import com.agilehealth.ui.dashboard_fragment.model.ReplyLikeResponse;
import com.agilehealth.ui.dashboard_fragment.model.ReplyLikeUpdate;
import com.agilehealth.ui.forgotpassword.model.ForgotRequest;
import com.agilehealth.ui.forgotpassword.model.ForgotResponse;
import com.agilehealth.ui.login.model.LoginRequest;
import com.agilehealth.ui.login.model.Model;
import com.agilehealth.ui.paswordupdate.model.ChangeModel;
import com.agilehealth.ui.paswordupdate.model.ChangeResponse;
import com.agilehealth.ui.profilefragment.model.UpdateResponse;
import com.agilehealth.ui.register.model.Register;
import com.agilehealth.ui.register.model.RegisterResponse;
import com.agilehealth.ui.main.model.LogoutResponse;
import com.agilehealth.ui.supportfragment.model.SupportModel;
import com.agilehealth.ui.supportfragment.model.SupportResponse;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

/**
 * Administrator on 10-May-17.
 */

public interface ApiService
{

    @POST("UserRegister")
    Call<RegisterResponse> register(@Body Register register);

    @POST("login")
    Call<Model> login(@Body LoginRequest loginRequest);

    @POST("forgotpassword")
    Call<ForgotResponse>reset(@Body ForgotRequest forgotRequest);

    @GET("logout/{Id}")
    Call<LogoutResponse>logout(@Path("Id") String id);

    @POST("updatePassword")
    Call<ChangeResponse>ChangePassword(@Body ChangeModel changeModel);

    @POST("sendmail")
    Call<SupportResponse>Support(@Body SupportModel supportModel);

    @Multipart
    @POST("imageUpload")
    Call<UpdateResponse>updateProfile(@PartMap HashMap<String, RequestBody> updateMap, @Part MultipartBody.Part profileImage);

    @POST("parkingLot")
    Call<MostPopularResponse> loadMostPopular(@Body MostPopular mostPopular);

    @POST("postMessage")
    Call<PostResponse> setPost(@Body PostRequest postRequest);

    @POST("commentLikeupdate")
    Call<CommentLikeResponse> getLikeUpdate(@Body CommentLikeUpdate commentLikeUpdate);

    @POST("replyLikeupdate")
    Call<ReplyLikeResponse> getreplyLikeUpdate(@Body ReplyLikeUpdate replyLikeUpdate);


}
