package com.agilehealth.data.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.agilehealth.R;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {
    private static final String EXTRA_MIN_MILLIS = "EXTRA_MIN_MILLIS";
    private static final String EXTRA_MAX_MILLIS = "EXTRA_MAX_MILLIS";
    private Calendar mDate;
    private int year, month, day, hour, min;
    private DateTimeChangeCallback callback;
    private long minMillis, maxMillis;

    public static DatePickerFragment newInstance(long minMillis, long maxMillis) {
        Bundle args = new Bundle();
        args.putLong(EXTRA_MIN_MILLIS, minMillis);
        args.putLong(EXTRA_MAX_MILLIS, maxMillis);
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        minMillis = args.getLong(EXTRA_MIN_MILLIS);
        maxMillis = args.getLong(EXTRA_MAX_MILLIS);
    }

    public void setCallback(DateTimeChangeCallback callback) {
        this.callback = callback;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //   mDate = (Date)getArguments().getSerializable(EXTRA_DATE);
        final Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        min = calendar.get(Calendar.MINUTE);

        View v = getActivity().getLayoutInflater().inflate(R.layout.time_picker, null);

        DatePicker datePicker = (DatePicker) v.findViewById(R.id.datePicker);
        TimePicker timePicker = (TimePicker) v.findViewById(R.id.timePicker);

        datePicker.init(
                year,
                month,
                day,
                new DatePicker.OnDateChangedListener() {
                    public void onDateChanged(DatePicker view, int year, int month, int day) {
                        DatePickerFragment.this.year = year;
                        DatePickerFragment.this.month = month;
                        DatePickerFragment.this.day = day;
                        updateDateTime();
                    }
                });
        if (minMillis > 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(minMillis);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            datePicker.setMinDate(cal.getTimeInMillis());

            year = cal.get(Calendar.YEAR);
            month = cal.get(Calendar.MONTH);
            day = cal.get(Calendar.DAY_OF_MONTH);
        }
        if (maxMillis > 0) {
            datePicker.setMaxDate(maxMillis);
        }

        updateDateTime();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker.setHour(hour);
            timePicker.setMinute(min);
        } else {
            timePicker.setCurrentHour(hour);
            timePicker.setCurrentMinute(min);
        }

        timePicker.setOnTimeChangedListener(
                new TimePicker.OnTimeChangedListener() {
                    public void onTimeChanged(TimePicker view, int hour, int min) {
                        DatePickerFragment.this.hour = hour;
                        DatePickerFragment.this.min = min;
                        updateDateTime();
                    }
                });

        builder.setView(v);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callback != null) {
                            callback.onDateTimeChanged(mDate);
                            dismiss();
                        }
                    }
                });
        builder.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
        return builder.create();
    }

    public void updateDateTime() {
        if (mDate == null) {
            mDate = Calendar.getInstance();
        }
        mDate.set(Calendar.YEAR, year);
        mDate.set(Calendar.MONTH, month);
        mDate.set(Calendar.DAY_OF_MONTH, day);
        mDate.set(Calendar.HOUR_OF_DAY, hour);
        mDate.set(Calendar.MINUTE, min);
        //  getArguments().putSerializable(EXTRA_DATE, mDate);
    }

    public interface DateTimeChangeCallback {
        void onDateTimeChanged(Calendar calendar);
    }
}
