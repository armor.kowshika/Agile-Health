package com.agilehealth.data.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.agilehealth.R;
import com.agilehealth.util.camera.PickerBuilder;
import com.agilehealth.util.camera.PickerManager;
import com.balysv.materialripple.MaterialRippleLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Administrator on 04-May-17.
 */
public class PhotoChooserDialog extends BottomSheetDialogFragment
        implements PickerBuilder.onImageReceivedListener
{
    @BindView(R.id.btnCamera)
    MaterialRippleLayout btnCamera;

    @BindView(R.id.delete)
    MaterialRippleLayout delete;

    @BindView(R.id.btnGallery)
    MaterialRippleLayout btnGallery;
    private boolean withTimeStamp = true;
    private String folder = null;
    private String imageName;
    protected Activity activity;

    Unbinder unbinder;
    private PickerBuilder pickerCamera = null, pickerGallery = null, pickerdelte=null;
    PickerManager pickerManager;

    private ImageCallback imageCallback;
    private ImageCallbackDel imageCallbackDel;
    private Callback callback;
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback=
            new BottomSheetBehavior.BottomSheetCallback() {

                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                       // dismiss();
                        dismissAllowingStateLoss();
                    }

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                   // dismiss();
                    dismissAllowingStateLoss();
                }
            };

    public void setImageCallback(ImageCallback imageCallback)
    {
        this.imageCallback = imageCallback;
    }
    public void setCallback(Callback callback)
    {
        this.callback=callback;
    }
public void setImageCallbackDel(ImageCallbackDel imageCallbackDel)
{
    this.imageCallbackDel=imageCallbackDel;
}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pickerCamera =
                new PickerBuilder(getActivity(), PickerBuilder.SELECT_FROM_CAMERA)
                        .setOnImageReceivedListener(this)
                        .setImageFolderName(getString(R.string.app_name))
                        .setCropScreenColor(
                                ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                        .withTimeStamp(true)
                        .setCropScreenColor(
                                ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        pickerGallery =
                new PickerBuilder(getActivity(), PickerBuilder.SELECT_FROM_GALLERY)
                        .setOnImageReceivedListener(this)
                        .setImageFolderName(getString(R.string.app_name))
                        .setCropScreenColor(
                                ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                        .withTimeStamp(true)
                        .setCropScreenColor(
                                ContextCompat.getColor(getActivity(), R.color.colorPrimary));


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
       View contentView = View.inflate(getContext(), R.layout.dialog_photo_chooser, null);
        dialog.setContentView(contentView);
        unbinder = ButterKnife.bind(this, contentView);
        attachListener();
        CoordinatorLayout.LayoutParams params =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();

        CoordinatorLayout.Behavior behavior = params.getBehavior();

       /* if (behavior != null && behavior instanceof BottomSheetBehavior) {*/
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);


            /*// We treat the CoordinatorLayout as outside the dialog though it is technically inside
            // if (shouldWindowCloseOnTouchOutside()) {
                contentView.findViewById(R.id.touch_outside).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (isVisible()) {
                                    dismiss();
                                }
                            }
                        });
            }
            return;*/
        }


  /*  private boolean shouldWindowCloseOnTouchOutside() {
        if (Build.VERSION.SDK_INT < 11) {
            return true;
        }
        TypedValue value = new TypedValue();
        //noinspection SimplifiableIfStatement
        if (getContext().getTheme()
                .resolveAttribute(android.R.attr.windowCloseOnTouchOutside, value, true)) {
            return value.data != 0;
        }
        return false;
    }*/



    private void attachListener() {
        btnCamera.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pickerCamera.start();
                    }
                });
        btnGallery.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pickerGallery.start();
                    }
                });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                removeImage();



            }
        });
    }

    @Override
    public void onImageReceived(Uri imageUri) {
        if (imageCallback != null) {
            imageCallback.onImageReceived(imageUri);
            dismissAllowingStateLoss();
        }
    }
    public void onImageReceivedDel(Uri imageUri)
    {
        if (imageCallback != null) {
            imageCallback.onImageReceived(imageUri);
            dismissAllowingStateLoss();
        }
    }


    public void removeImage()
    {
        if (callback != null)
        {
            callback.removeImage();
            dismissAllowingStateLoss();
        }
    }



    public interface ImageCallback
    {

        void onImageReceived(Uri imageUri);
    }

    public interface ImageCallbackDel
    {

        void onImageReceivedDel(Uri imageUri);
    }
    public interface Callback
    {
        void removeImage();
    }

}
