package com.agilehealth.data.util;

import android.app.ProgressDialog;
import android.content.Context;

import com.agilehealth.R;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public class ProgressUtils
{
    private ProgressDialog progressDialog;
    private String defaultMsg;

    public ProgressUtils(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        defaultMsg = context.getString(R.string.loading);
        progressDialog.setMessage(defaultMsg);
    }

    public ProgressDialog getProgress() {
        return progressDialog;
    }

    public void setMessage(String msg) {
        if (progressDialog != null) {
            progressDialog.setMessage(msg);
        }
    }

    public void show() {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.setMessage(defaultMsg);
                progressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismiss() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
