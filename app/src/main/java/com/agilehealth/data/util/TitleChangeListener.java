package com.agilehealth.data.util;

/**
 * Created by ck-nivetha on 13/9/17.
 */

public interface TitleChangeListener {
    void changeTitle(String title);
}
