package com.agilehealth.data.fcm;

import android.util.Log;

import com.agilehealth.data.PrefConnect;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Administrator on 11-May-17.
 */

public class MyInstanceId extends FirebaseInstanceIdService {

    private static final String TAG = "MyInstanceId";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        PrefConnect.writeString(this, PrefConnect.FCM_TOKEN, refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
    }
}
