package com.agilehealth.data.global;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.SpannableString;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created on 6/15/2017.
 */

public class Global {

    public static final String FCM_TOKEN = "FCM_TOKEN";
    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final String MOBILE_PATTERN = "^[789]\\d{9}$";
    public static final int PERMISSION_REQUEST = 1;

    public static void Toast(Context context, String Message) {
        SpannableString spannableString = new SpannableString(Message);
        Toast toast = Toast.makeText(context, spannableString, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static String getFormattedDate(int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd yyyy");
        return sdf.format(date);
    }

    public static String getDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date tempDate = null;
        try {
            tempDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newFormat.format(tempDate);
    }

    public static String getCurrentDate() {
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static String getCurrentDateTime() {
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        TimeZone tz = TimeZone.getTimeZone("EST");
        sdf.setTimeZone(tz);
        return sdf.format(date);
    }

    public static String formateESTtime(String time){
        SimpleDateFormat newformat = new SimpleDateFormat("yyyy-MM-dd");
        Date date=null;
        try {
            date=newformat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TimeZone tz = TimeZone.getTimeZone("EST");
        newformat.setTimeZone(tz);
        System.out.println("Current Date Time : " + newformat.format(date));

        return newformat.format(date);
    }

    public static String formatedDate(String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM HH:MM:SS");
        SimpleDateFormat newFormat = new SimpleDateFormat("MM/dd/yyyy, hh:mm aa");
        Date tempDate = null;
        try {
            tempDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newFormat.format(tempDate);
    }

    public static String Date(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date tempDate = null;
        try {
            tempDate = newFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormat.format(tempDate);
    }

    /*public static String estTimeFormat(String date){
  //      Calendar currentdate = Calendar.getInstance();
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date tempDate = null;
        try {
            tempDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TimeZone obj = TimeZone.getTimeZone("EST");
        formatter.setTimeZone(obj);
        System.out.println("Local:: " +tempDate.getTime());
        System.out.println("CST:: "+ formatter.format(tempDate.getTime()));
        return formatter.format(tempDate.getTime());
    }*/

    public static String dateFormat(String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM HH:MM:SS");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-dd-MM, hh:mm:ss");
        Date tempDate = null;
        try {
            tempDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newFormat.format(tempDate);
    }

    public static String getTime(String time) {
        String convertedTime = "";
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-dd-MM, hh:mm:ss");

        try {
            TimeZone timeZone = TimeZone.getDefault();
            Date postdate = sdf.parse(time);
            long postTimeStamp = postdate.getTime() + timeZone.getRawOffset();

            String dateString = sdf.format(new Date(postTimeStamp));

            convertedTime = dateString;


            // convertedTime = getLastTime(context, time);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedTime;
    }

    public static String dateTime(String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM HH:MM:SS");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
        Date tempDate = null;
        try {
            tempDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newFormat.format(tempDate);
    }

    public static Date date(String s) {
        DateFormat format=new SimpleDateFormat("yyyy-dd-MM hh:mm:ss aa");
        Date date=null;
        try {
            date = format.parse(s);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;


    }

    public static String datetoString(Date currentdate) {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa" );
        String datetime = dateformat.format(currentdate);
        System.out.println("Current Date Time : " + datetime);
        return datetime;
    }

}
