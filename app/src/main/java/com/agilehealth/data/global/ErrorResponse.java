package com.agilehealth.data.global;

/**
 * Created by armor-selva on 27/6/17.
 */

public class ErrorResponse {

    public static final int EMAIL_EXISTS = 420;
    public static final int MOBILE_EXISTS = 421;
    public static final int INVALID_CREDENTIAL = 422;

}
