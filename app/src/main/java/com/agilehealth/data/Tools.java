/**
 * Copyright 2013 Google Inc. All Rights Reserved.
 * <p>
 * <p>Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain edt_search_bg copy of the License at
 * <p>
 * <p>http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * <p>Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.agilehealth.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Provides utility functions.
 */
public class Tools {

    public static final String TAG = "Tools";

    public static final String FIREBASE = "https://flickering-inferno-313.firebaseio.com/";

    //public static final String DATE_FORMAT = "yyyy-MM-dd";
    //public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String MIME_TYPE_TEXT = "text/plain";

    public static final String MESSAGE_TYPE_SENT = "SENT";
    public static final String MESSAGE_TYPE_RECEIVED = "RECEIVED";

    public static final SimpleDateFormat FORM_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy MMM dd");
    public static final SimpleDateFormat REVERSE_DATE_FORMAT = new SimpleDateFormat("MMM dd yyyy");
    public static final SimpleDateFormat DEFAULT_MON_DAY_FORMAT = new SimpleDateFormat("MMM dd");
    public static final SimpleDateFormat DEFAULT_DAY_FORMAT = new SimpleDateFormat("dd");
    public static final SimpleDateFormat DEFAULT_MON_YEAR_FORMAT = new SimpleDateFormat("MMM yyyy");
    public static final SimpleDateFormat DEFAULT_TIME_FORMAT = new SimpleDateFormat("HH:mm a");
    public static final SimpleDateFormat DEFAULT_DATE_TIME_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat COMMENT_DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat CHAT_DATE_FORMAT = new SimpleDateFormat("h:mm a");
    public static final SimpleDateFormat GAE_DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public static final String AB =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    public static final HashMap<String, String> seqMap = new HashMap<>();
    public static final String[] months =
            new String[]{
                    "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "JAN", "FEB", "MAR", "APR", "MAY"
            };
    public static SecureRandom secureRandom = new SecureRandom();

    public static String readFromInputStream(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        return builder.toString();
    }

    public static boolean checkNulls(String string) {
        try {
            if (string == null) return false;
            return (!string.equals("null") && !string.equals(null) && !string.equals(""));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean checkNulls(Long l) {
        try {
            if (l == null) return false;
            if (l == 0) return false;
            return checkNulls("" + l);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean checkNulls(Integer i) {
        try {
            if (i == null) return false;
            if (i == 0) return false;
            return checkNulls("" + i);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean checkNulls(Double d) {
        try {
            if (d == null) return false;
            if (d == 0) return false;
            return checkNulls("" + d);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean checkNulls(Date date) {
        try {
            if (date == null) return false;
            return checkNulls("" + date);
        } catch (Exception e) {
            return false;
        }
    }

    public static String getMimeType(String fileUrl) throws IOException {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        return fileNameMap.getContentTypeFor(fileUrl);
    }

    public static String getTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        return sdf.format(date);
    }

    public static String getUrlBySize(String s, int dimension) {
        //String s = "https://lh5.googleusercontent.com/J4uVXW1NRzcvAULLR-yZEPfScObdcDUJSyW1fimlgdfp3xaZwuxzomzjlwDlJp-K_Z4FSQ=s220";
        //Log.e(TAG, "s " + s);
        String size = s.substring(s.lastIndexOf("=s"));
        //Log.e(TAG, "size " + size);
        String url = s.substring(0, s.lastIndexOf(size));
        //Log.e(TAG, "B url " + url);
        url = url + "=s" + dimension;
        //url = url + "=s20";
        Log.e("Tools", "url " + url);
        return url;
    }

    public static String getDurationMark(String filePath) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(filePath);
        } catch (Exception e) {
            Log.e("getDurationMark", e.toString());
            return "?:??";
        }
        String time = null;

        try {
            Log.e("file", filePath);
            time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        } catch (Exception ex) {
            Log.e("getDurationMark", ex.toString());
        }

        time = time == null ? "0" : time.isEmpty() ? "0" : time;
        //bam crash - no more :)
        int timeInMillis = Integer.parseInt(time);
        int duration = timeInMillis / 1000;
        int hours = duration / 3600;
        int minutes = (duration % 3600) / 60;
        int seconds = duration % 60;
        StringBuilder sb = new StringBuilder();
        if (hours > 0) {
            sb.append(hours).append(":");
        }
        if (minutes < 10) {
            sb.append("0").append(minutes);
        } else {
            sb.append(minutes);
        }
        sb.append(":");
        if (seconds < 10) {
            sb.append("0").append(seconds);
        } else {
            sb.append(seconds);
        }
        return sb.toString();
    }

    public static Long checkDuration(String filePath) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(filePath);
        } catch (Exception e) {
            Log.e("checkDuration", e.toString());
            return 0L;
        }
        String time = null;

        try {
            Log.e("file", filePath);
            time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        } catch (Exception ex) {
            Log.e("checkDuration", ex.toString());
        }

        time = time == null ? "0" : time.isEmpty() ? "0" : time;
        //bam crash - no more :)
        return Long.parseLong(time);
    }

    //    public static int checkViewType(int left, String contentType){
    //        int viewType = 0;
    //        if(left == 1){
    //            if(contentType.contains("application")){
    //                viewType = Item.TYPE_SELF_FILE;
    //            } else if(contentType.equalsIgnoreCase("TEXT")){
    //                viewType = Item.TYPE_SELF_TEXT;
    //            } else if(contentType.contains("image")){
    //                viewType = Item.TYPE_SELF_IMAGE;
    //            } else if(contentType.contains("video")){
    //                viewType = Item.TYPE_SELF_VIDEO;
    //            } else {
    //                viewType = Item.TYPE_SELF_FILE;
    //            }
    //        } else {
    //            if(contentType.contains("application")){
    //                viewType = Item.TYPE_OTHER_FILE;
    //            } else if(contentType.equalsIgnoreCase("TEXT")){
    //                viewType = Item.TYPE_OTHER_TEXT;
    //            } else if(contentType.contains("image")){
    //                viewType = Item.TYPE_OTHER_IMAGE;
    //            } else if(contentType.contains("video")){
    //                viewType = Item.TYPE_OTHER_VIDEO;
    //            } else {
    //                viewType = Item.TYPE_OTHER_FILE;
    //            }
    //        }
    //        return viewType;
    //    }
    //
    //    public static int checkMessageStatus(int status, int viewType){
    //        switch (status){
    //            case 1:
    //                if (viewType == Item.TYPE_SELF_TEXT || viewType == Item.TYPE_SELF_FILE) {
    //                    return R.drawable.message_got_receipt_from_server;
    //                } else {
    //                    return R.drawable.message_got_receipt_from_server_onmedia;
    //                }
    //            case 2:
    //                if (viewType == Item.TYPE_SELF_TEXT || viewType == Item.TYPE_SELF_FILE) {
    //                    return R.drawable.message_got_receipt_from_target;
    //                } else {
    //                    return R.drawable.message_got_receipt_from_target_onmedia;
    //                }
    //            case 3:
    //                if (viewType == Item.TYPE_SELF_TEXT || viewType == Item.TYPE_SELF_FILE) {
    //                    return R.drawable.message_got_read_receipt_from_target;
    //                } else {
    //                    return R.drawable.message_got_read_receipt_from_target_onmedia;
    //                }
    //            default:
    //                if (viewType == Item.TYPE_SELF_TEXT || viewType == Item.TYPE_SELF_FILE) {
    //                    return R.drawable.message_unsent;
    //                } else {
    //                    return R.drawable.message_unsent_onmedia;
    //                }
    //        }
    //    }

    public static String getClearTime(long m, long s) {
        m = m >= 0 ? m : 0;
        s = s >= 0 ? s : 0;
        return (m > 9 ? m : "0" + m) + ":" + (s > 9 ? s : "0" + s);
    }

    private static String getClearTime(long h, long m, long s) {
        h = h >= 0 ? h : 0;
        m = m >= 0 ? m : 0;
        s = s >= 0 ? s : 0;
        return (h > 9 ? h : "0" + h) + ":" + (m > 9 ? m : "0" + m) + ":" + (s > 9 ? s : "0" + s);
    }

    private static String getClearTime(long d, long h, long m, long s) {
        d = d >= 0 ? d : 0;
        h = h >= 0 ? h : 0;
        m = m >= 0 ? m : 0;
        s = s >= 0 ? s : 0;
        return (d > 9 ? d : "0" + d)
                + ":"
                + (h > 9 ? h : "0" + h)
                + ":"
                + (m > 9 ? m : "0" + m)
                + ":"
                + (s > 9 ? s : "0" + s);
    }

    public static String getHMS(long diff) {
        long diffSeconds = 0, diffMinutes = 0, diffHours = 0, diffDays = 0;
        diffDays = diff / (24 * 60 * 60 * 1000);
        diffHours = diff / (60 * 60 * 1000) % 24;
        diffMinutes = diff / (60 * 1000) % 60;
        diffSeconds = diff / 1000 % 60;
        if (diffDays > 0) {
            if (diffDays > 10000) {
                return "";
            } else {
                return getClearTime(diffDays, diffHours, diffMinutes, diffSeconds);
            }
        } else if (diffHours > 0) {
            return getClearTime(diffHours, diffMinutes, diffSeconds);
        } else {
            return getClearTime(diffMinutes, diffSeconds);
        }
    }

    public static Bitmap getBitmapFromDisk(String path) {
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 100000; // 100KB
            //final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = new FileInputStream(new File(path));

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            //o.inJustDecodeBounds = true;
            o.inJustDecodeBounds = false;
            o.inPreferredConfig = Bitmap.Config.RGB_565;
            o.inDither = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();

            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d(
                    TAG,
                    "scale = "
                            + scale
                            + ", orig-width: "
                            + o.outWidth
                            + ", orig-height: "
                            + o.outHeight);

            Bitmap b = null;
            in = new FileInputStream(new File(path));
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                o.inJustDecodeBounds = false;
                o.inPreferredConfig = Bitmap.Config.RGB_565;
                o.inDither = true;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d(
                        TAG,
                        "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d(TAG, "bitmap size - width: " + b.getWidth() + ", height: " + b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }
    }

    public static String getRandomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) sb.append(AB.charAt(secureRandom.nextInt(AB.length())));
        return sb.toString();
    }

    public static String convertStreamToString(InputStream is) {
        ByteArrayOutputStream oas = new ByteArrayOutputStream();
        copyStream(is, oas);
        String t = oas.toString();
        try {
            oas.close();
            oas = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return t;
    }

    private static void copyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1) break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
