package com.agilehealth.base;

/**
 * Administrator on 04-Apr-17.
 */

public interface BaseView {
    void setUpToolbar();

    void showProgress();

    void hideProgress();

}
