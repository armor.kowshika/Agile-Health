package com.agilehealth.base;

/**
 * Administrator on 04-Apr-17.
 */

public interface Presenter<T extends BaseView> {

    void attach(T t);

    void detach();
}
