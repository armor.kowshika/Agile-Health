package com.agilehealth.base;

import com.google.gson.annotations.Expose;

/**
 * Administrator on 10-Apr-17.
 */

public class BaseResult {
    @Expose
    protected String status;
    @Expose
    protected String message;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
