package com.agilehealth.base;

/**
 * Administrator on 04-Apr-17.
 */

public abstract class BasePresenter<T extends BaseView> implements Presenter<T> {

    private T t;

    @Override
    public void attach(T t) {
        this.t = t;
    }

    public T getView() {
        return t;
    }

    @Override
    public void detach() {
        t = null;
    }
}
