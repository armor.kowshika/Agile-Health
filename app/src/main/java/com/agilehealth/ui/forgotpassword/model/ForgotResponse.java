package com.agilehealth.ui.forgotpassword.model;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public class ForgotResponse {

    public String Result;
    public String Status;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
