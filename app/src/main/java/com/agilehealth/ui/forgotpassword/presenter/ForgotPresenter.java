package com.agilehealth.ui.forgotpassword.presenter;

import com.agilehealth.base.Presenter;
import com.agilehealth.ui.forgotpassword.model.ForgotRequest;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public interface ForgotPresenter extends Presenter<ForgotView> {

    void resetPassword(ForgotRequest forgotPassword);
}
