package com.agilehealth.ui.forgotpassword.presenter;

import com.agilehealth.base.BaseView;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public interface ForgotView extends BaseView
{
    void onResetFailed(String error);

    void onResetSucess();
}
