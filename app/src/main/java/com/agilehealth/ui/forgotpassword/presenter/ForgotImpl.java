package com.agilehealth.ui.forgotpassword.presenter;

import android.util.Log;

import com.agilehealth.base.BasePresenter;
import com.agilehealth.data.api.ApiService;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.injection.module.ApiModule;
import com.agilehealth.ui.forgotpassword.model.ForgotRequest;
import com.agilehealth.ui.forgotpassword.model.ForgotResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public class ForgotImpl extends BasePresenter<ForgotView>implements ForgotPresenter
{
    @Named(ApiModule.AUTH)
    @Inject
    ApiService apiService;

    @Override
    public void attach(ForgotView forgotView)
    {
        super.attach(forgotView);
        DaggerAppComponent.create().inject(this);
        getView().setUpToolbar();
    }

    @Override
    public void resetPassword(ForgotRequest forgotPassword)
    {
        getView().showProgress();
        Call<ForgotResponse>call=apiService.reset(forgotPassword);
        call.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                getView().hideProgress();
                if(response.isSuccessful())
                {

                    if(response.body().getStatus().equals("Password reset link has been sent to your mail"))
                    {
                        getView().onResetSucess();
                    }
                    else if(response.body().getStatus().equals("User not found"))
                    {
                        getView().onResetFailed("Not Registered Email");
                    }
                }
                else
                {
                    getView().onResetFailed("Not sucessfull");
                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t)
            {
                getView().hideProgress();
                getView().onResetFailed("Slow internet connection");
                Log.e(TAG, t.toString());
            }
        });

    }
}
