package com.agilehealth.ui.forgotpassword.model;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public class ForgotRequest {

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String email;
}
