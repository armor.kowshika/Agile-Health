package com.agilehealth.ui.forgotpassword;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.agilehealth.R;
import com.agilehealth.base.BaseActivity;
import com.agilehealth.data.global.Global;
import com.agilehealth.data.util.ProgressUtils;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.ui.forgotpassword.model.ForgotRequest;
import com.agilehealth.ui.forgotpassword.presenter.ForgotPresenter;
import com.agilehealth.ui.forgotpassword.presenter.ForgotView;
import com.agilehealth.util.toast.ToastMaker;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotAcitivity extends BaseActivity implements ForgotView {


    @Inject
    ForgotPresenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtEmail)
    AppCompatEditText edtEmail;
    @BindView(R.id.btnSubmit)
    AppCompatButton btnSubmit;
    private ProgressUtils progressUtils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgotpassword);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        progressUtils = new ProgressUtils(this);
        presenter.attach(this);

    }

    @Override
    public void isNetworkAvailable(boolean isAvailable)
    {
        if (!isAvailable)
        {
            ToastMaker.makeToast(this, "No internet connection");
        }
    }

    @Override
    public void setUpToolbar()
    {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }

    @OnClick({R.id.btnSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btnSubmit:

                if (edtEmail.getText().toString().isEmpty()) {
                    Global.Toast(ForgotAcitivity.this, "Enter  Email");
                } else if (!edtEmail.getText().toString().trim().matches(Global.EMAIL_PATTERN)) {
                    Global.Toast(ForgotAcitivity.this, "Enter Valid Email");
                } else {
                    ForgotRequest forgotPassword = new ForgotRequest();
                    forgotPassword.setEmail(edtEmail.getText().toString().trim());
                    presenter.resetPassword(forgotPassword);
                }
                break;
        }

        }



            @Override
    public void showProgress()
            {
progressUtils.show();
    }

    @Override
    public void hideProgress() {
progressUtils.dismiss();
    }

    @Override
    public void onResetFailed(String error)
    {
        ToastMaker.makeToast(this, error);
    }

    @Override
    public void onResetSucess()
    {
        ToastMaker.makeToast(this,"Password reset link has been sent to your mail");
    }
}
