package com.agilehealth.ui.login.googlesign;

/**
 * Created by ck-nivetha on 11/9/17.
 */

public class Constants {
    public static final String FIREBASE_URL = "https://console.firebase.google.com/project/agilehealth-560eb/authentication";
    public static final String FIREBASE_LOCATION_USERS = "providers";
    public static final String FIREBASE_URL_USERS = FIREBASE_URL + "/" + FIREBASE_LOCATION_USERS;
    public static final String FIREBASE_PROPERTY_TIMESTAMP = "timestamp";
}
