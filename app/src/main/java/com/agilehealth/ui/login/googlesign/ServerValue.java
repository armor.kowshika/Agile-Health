package com.agilehealth.ui.login.googlesign;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ck-nivetha on 11/9/17.
 */

public class ServerValue {
    public static final Map<String, String> TIMESTAMP = zzsc("timestamp");

    public ServerValue() {
    }

    private static Map<String, String> zzsc(String var0) {
        HashMap var1 = new HashMap();
        var1.put(".sv", var0);
        return Collections.unmodifiableMap(var1);
    }
}
