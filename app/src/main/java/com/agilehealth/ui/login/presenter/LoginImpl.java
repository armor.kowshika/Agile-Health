package com.agilehealth.ui.login.presenter;

import android.util.Log;

import com.agilehealth.app.App;
import com.agilehealth.base.BasePresenter;
import com.agilehealth.data.api.ApiService;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.injection.module.ApiModule;
import com.agilehealth.session.Profile;
import com.agilehealth.ui.login.model.LoginRequest;
import com.agilehealth.ui.login.model.Model;
import com.agilehealth.ui.register.model.RegisterResponse;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public class LoginImpl extends BasePresenter<LoginView>implements LoginPresenter {
    @Named(ApiModule.DEFAULT)
    @Inject
    ApiService apiService;



    @Override
    public void attach(LoginView loginView) {
        super.attach(loginView);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void goToLogin(LoginRequest login)
    {
        getView().showProgress();
        Log.e("goToLogin", "beforecall  ");
        Call<Model> call = apiService.login(login);
        call.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {

                getView().hideProgress();


                if (response.isSuccessful())
                {


                    Log.e("objec","reference"+response.body().getStatus() );
                    if (response.body().getStatus().equals("Invalid username"))
                    {
                        getView().Show("Invalid Username");
                    }
                    else if(response.body().getStatus().equals("Login successfully"))
                    {
                       Model.Details details=response.body().getUserdetails();
                        Profile profile = new Profile();
                        profile.setId(Long.parseLong("" + details.getId()));
                        Log.e("id"," "+profile.getId());
                        profile.setFirstName(details.getFirstName());
                        profile.setLastName(details.getLastName());
                        profile.setEmail(details.getEmail());
                        profile.setMobile(details.getMobile());
                        profile.setProfileStatus(details.getProfileStatus());
                        profile.setAuthToken(details.getAuthToken());
                        profile.setPassword(details.getPassword());
                        profile.setProfilePicture(details.getProfilePicture());
                        Log.e(TAG,"picture" +details.getProfilePicture() );
                        profile.setDeviceType(details.getDeviceType());
                        profile.setTitle(details.getTitle());
                        profile.setAgileCertification(details.getAgileCertification());
                        profile.setCompany(details.getCompany());
                        profile.setIsLogin(details.getIsLogin());
                        App.getInstance().profileDao.insertOrReplace(profile);

                        Log.e(TAG,"profile " + profile.id );

                        List<Profile> profileList = App.getInstance().profileDao.queryBuilder().list();
                        if (profileList != null) {
                            Log.e(TAG,"profileList " + profileList.size() );
                        } else {
                            Log.e(TAG,"profileList null");
                        }

                        getView().Sucess();
                    }
                }
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t)
            {
                getView().hideProgress();
                getView().onLoginFailed("Slow internet connection");
                Log.e(TAG, t.toString());
            }
        });
    }
}
