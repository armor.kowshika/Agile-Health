package com.agilehealth.ui.login;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.agilehealth.R;
import com.agilehealth.base.BaseActivity;
import com.agilehealth.data.PrefConnect;
import com.agilehealth.data.global.Global;
import com.agilehealth.data.util.ProgressUtils;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.ui.forgotpassword.ForgotAcitivity;
import com.agilehealth.ui.login.googlesign.SharedPrefManager;
import com.agilehealth.ui.login.model.LoginRequest;
import com.agilehealth.ui.login.presenter.LoginPresenter;
import com.agilehealth.ui.login.presenter.LoginView;
import com.agilehealth.ui.register.RegisterActivity;
import com.agilehealth.ui.main.MainActivity;
import com.agilehealth.util.toast.ToastMaker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView,
GoogleApiClient.OnConnectionFailedListener {

    @Inject
    LoginPresenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtUserName)
    AppCompatEditText edtUserName;
    @BindView(R.id.username)
    TextInputLayout username;
    @BindView(R.id.edtPassword)
    AppCompatEditText edtPassword;
    @BindView(R.id.txtForgotPassword)
    AppCompatTextView txtForgotPassword;
    @BindView(R.id.btnLogin)
    AppCompatButton btnLogin;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.btnGoogle)
    AppCompatImageButton btnGoogle;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.googletext)
    AppCompatTextView googletext;
    @BindView(R.id.navLayout)
    RelativeLayout navLayout;
    @BindView(R.id.newUser)
    AppCompatTextView newUser;
    @BindView(R.id.textregister)
    RelativeLayout textregister;
    @BindView(R.id.register)
    AppCompatTextView register;
    private ProgressUtils progressUtils;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "MainActivity";
    private String idToken;
    private final Context mContext = this;

    private String name, email;
    private String photo;
    private Uri photoUri;
    public SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attach(this);
        progressUtils = new ProgressUtils(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
       /* configureSignIn();

        mAuth = com.google.firebase.auth.FirebaseAuth.getInstance();
*/
       // this is where we start the Auth state Listener to listen for whether the user is signed in or not
       /* mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                // Get signedIn user
                FirebaseUser user = firebaseAuth.getCurrentUser();

                //if user is signed in, we call a helper method to save the user details to Firebase
                if (user != null) {
                    // User is signed in
                    createUserInFirebaseHelper();
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };*/
    }

   /* private void createUserInFirebaseHelper(){

        //Since Firebase does not allow "." in the key name, we'll have to encode and change the "." to ","
        // using the encodeEmail method in class Utils
        final String encodedEmail = Utils.encodeEmail(email.toLowerCase());

        //create an object of Firebase database and pass the the Firebase URL
        final Firebase userLocation = new Firebase(Constants.FIREBASE_URL_USERS).child(encodedEmail);

        //Add a Listerner to that above location
        userLocation.addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null){
                    *//* Set raw version of date to the ServerValue.TIMESTAMP value and save into dateCreatedMap *//*
                    HashMap<String, Object> timestampJoined = new HashMap<>();
                    timestampJoined.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);

                    // Insert into Firebase database
                    User newUser = new User(name, photo, encodedEmail, timestampJoined);
                    userLocation.setValue(newUser);

                    Toast.makeText(LoginActivity.this, "Account created!", Toast.LENGTH_SHORT).show();

                    // After saving data to Firebase, goto next activity
//                    Intent intent = new Intent(MainActivity.this, NavDrawerActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                    finish();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.d(TAG, getString(R.string.log_error_occurred) + firebaseError.getMessage());
                //hideProgressDialog();
                if (firebaseError.getCode() == FirebaseError.EMAIL_TAKEN){
                }
                else {
                    Toast.makeText(LoginActivity.this, firebaseError.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }


        });
    }

    // This method configures Google SignIn
    public void configureSignIn(){
// Configure sign-in to request the user's basic profile like name and email
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(LoginActivity.this.getResources().getString(R.string.web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .enableAutoManage(this *//* FragmentActivity *//*, this *//* OnConnectionFailedListener *//*)
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();
        mGoogleApiClient.connect();
    }

    // This method is called when the signIn button is clicked on the layout
    // It prompts the user to select a Google account.
    private void signIn()
    {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    // This IS the method where the result of clicking the signIn button will be handled
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        Log.e("requestCode" ," request" +requestCode);

        Log.e("resultcode","result" + resultCode);

        Log.e("data", "result" + data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN)
        {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            Log.e("result","result" + result);

            if (result.isSuccess()) {
                // Google Sign In was successful, save Token and a state then authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();

                idToken = account.getIdToken();

                name = account.getDisplayName();
                email = account.getEmail();


                // Save Data to SharedPreference
                sharedPrefManager = new SharedPrefManager(mContext);
                sharedPrefManager.saveIsLoggedIn(mContext, true);

                sharedPrefManager.saveEmail(mContext, email);
                sharedPrefManager.saveName(mContext, name);
                sharedPrefManager.saveToken(mContext, idToken);
                //sharedPrefManager.saveIsLoggedIn(mContext, true);

                AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
                firebaseAuthWithGoogle(credential);
            } else
                {
                // Google Sign In failed, update UI appropriately
                Log.e(TAG, "Login Unsuccessful. ");
                Toast.makeText(this, "Login Unsuccessful", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    //After a successful sign into Google, this method now authenticates the user with Firebase
    private void firebaseAuthWithGoogle(AuthCredential credential){

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential" + task.getException().getMessage());
                            task.getException().printStackTrace();
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }else {
                            createUserInFirebaseHelper();
                            Toast.makeText(LoginActivity.this, "Login successful",
                                    Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                });
    }
*/

    @Override
    public void isNetworkAvailable(boolean isAvailable)
    {
        if (!isAvailable)
        {
            ToastMaker.makeToast(this, "No internet connection");
        }
    }

    @Override
    public void setUpToolbar()
    {
        setSupportActionBar(toolbar);
        toolbar.setTitle("Login");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

    }

   /* @Override
    protected void onStart() {
        super.onStart();
        if (mAuthListener != null){
            FirebaseAuth.getInstance().signOut();
        }
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
*/

    @Override
    public void showProgress()
    {
      progressUtils.show();
    }

    @Override
    public void hideProgress()
    {
      progressUtils.dismiss();
    }
    @OnClick({R.id.btnLogin,R.id.txtForgotPassword,R.id.register,R.id.googletext})
    public void onViewClicked(View view)
    {
        switch (view.getId()) {

            case R.id.btnLogin:
                if(validate())
                {
                    LoginRequest loginRequest=new LoginRequest();
                    loginRequest.setEmail(edtUserName.getText().toString());
                    loginRequest.setPassword(edtPassword.getText().toString());
                    loginRequest.setDeviceType("2");
                    Log.d("LoginActivity", "Refreshed token: " + PrefConnect.readString(this, PrefConnect.FCM_TOKEN, ""));
                    loginRequest.setDeviceToken(PrefConnect.readString(this, PrefConnect.FCM_TOKEN, ""));
                    loginRequest.setEnvironment("0");
                    presenter.goToLogin(loginRequest);
                }
                break;
            case R.id.txtForgotPassword:
                Intent intent=new Intent(LoginActivity.this,ForgotAcitivity.class);
                startActivity(intent);
                break;
            case R.id.register:
                Intent intent1=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent1);
                break;
            case R.id.googletext:

                signIn();

               /* Utils utils = new Utils(this);
                if (utils.isNetworkAvailable()){
                    signIn();
                }else {
                    Toast.makeText(LoginActivity.this, "Oops! no internet connection!", Toast.LENGTH_SHORT).show();
                }*/
                break;


               /* Intent intent=new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);*/
        }
        }
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            edtUserName.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    private void updateUI(boolean b)
    {

    }


    @Override
    public void onLoginFailed(String error)
    {
        ToastMaker.makeToast(this, error);
    }


        private boolean validate() {
        if (edtUserName.getText().toString().isEmpty()) {
            Global.Toast(LoginActivity.this, "Enter  Email");
            return false;

        } else if (edtPassword.getText().toString().isEmpty()) {
            Global.Toast(LoginActivity.this, "Enter password");
            return false;

        }
        return true;
    }



    @Override
    public void Sucess()
    {
        Intent intent=new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void Show(String message) {
        PrefConnect.writeBoolean(this,PrefConnect.LOGIN,true);
        ToastMaker.makeToast(this,message);
    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    {

    }
}
