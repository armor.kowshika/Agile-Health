package com.agilehealth.ui.login.presenter;

import com.agilehealth.base.Presenter;
import com.agilehealth.ui.login.model.LoginRequest;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public interface LoginPresenter extends Presenter<LoginView> {

    void goToLogin(LoginRequest login);
}
