package com.agilehealth.ui.login.presenter;

import com.agilehealth.base.BaseView;
import com.agilehealth.ui.login.model.Model;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public interface LoginView extends BaseView {
    void onLoginFailed(String error);

    void Sucess();

    void Show(String message);

}
