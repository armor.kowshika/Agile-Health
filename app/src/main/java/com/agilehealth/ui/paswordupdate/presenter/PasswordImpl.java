package com.agilehealth.ui.paswordupdate.presenter;

import com.agilehealth.base.BasePresenter;
import com.agilehealth.data.api.ApiService;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.injection.module.ApiModule;
import com.agilehealth.ui.paswordupdate.model.ChangeModel;
import com.agilehealth.ui.paswordupdate.model.ChangeResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ck-nivetha on 13/9/17.
 */

public class PasswordImpl extends BasePresenter<PasswordView>implements PasswordPresenter {

    @Named(ApiModule.DEFAULT)
    @Inject
    ApiService apiService;


    @Override
    public void attach(PasswordView passwordView) {
        super.attach(passwordView);
        DaggerAppComponent.create().inject(this);
        getView().setUpToolbar();
    }

    @Override
    public void ChangePassword(ChangeModel changeModel) {
        getView().showProgress();
        Call<ChangeResponse>call=apiService.ChangePassword(changeModel);
        call.enqueue(new Callback<ChangeResponse>() {
            @Override
            public void onResponse(Call<ChangeResponse> call, Response<ChangeResponse> response) {
                if(response.isSuccessful())
                {
                    if(response.body().getResult().equals("Success"))
                    getView().Sucess("Password Changed Sucessfully");

                  else  if(response.body().getMessage().equals("Customer not found"))
                    {
                        getView().Sucess("Customer not found");
                    }
                    else if(response.body().getMessage().equals("Old password does not match"))
                    {
                        getView().Sucess("Old password does not match");
                    }
                }

            }

            @Override
            public void onFailure(Call<ChangeResponse> call, Throwable t) {


            }
        });
    }
}
