package com.agilehealth.ui.paswordupdate;

import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.agilehealth.R;
import com.agilehealth.base.BaseActivity;
import com.agilehealth.data.PrefConnect;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.ui.paswordupdate.model.ChangeModel;
import com.agilehealth.ui.paswordupdate.presenter.PasswordPresenter;
import com.agilehealth.ui.paswordupdate.presenter.PasswordView;
import com.agilehealth.util.toast.ToastMaker;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordActivity extends BaseActivity implements PasswordView {

    @Inject
    PasswordPresenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtCurrent)
    AppCompatEditText edtCurrent;
    @BindView(R.id.relative)
    RelativeLayout relative;
    @BindView(R.id.edtNew)
    AppCompatEditText edtNew;
    @BindView(R.id.relativeNew)
    RelativeLayout relativeNew;
    @BindView(R.id.edtConf)
    AppCompatEditText edtConf;
    @BindView(R.id.relativeConf)
    RelativeLayout relativeConf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attach(this);
    }

    @Override
    public void isNetworkAvailable(boolean isAvailable) {

    }

    @Override
    public void setUpToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.changepasswordmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {

            case R.id.save:
                ChangeModel model=new ChangeModel();
                model.setId(PrefConnect.readString(this,PrefConnect.ID," "));
                model.setOldpassword(edtCurrent.getText().toString());
                model.setNewpassword(edtNew.getText().toString());
                presenter.ChangePassword(model);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void Sucess(String message) {
        ToastMaker.makeToast(this,message);
    }
}
