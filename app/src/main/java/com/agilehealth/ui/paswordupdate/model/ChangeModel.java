package com.agilehealth.ui.paswordupdate.model;

/**
 * Created by ck-nivetha on 13/9/17.
 */

public class ChangeModel {

    public String id;
    public String oldpassword;
    public String newpassword;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }
}
