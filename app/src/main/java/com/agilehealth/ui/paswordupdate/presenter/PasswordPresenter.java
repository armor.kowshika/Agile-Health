package com.agilehealth.ui.paswordupdate.presenter;

import com.agilehealth.base.Presenter;
import com.agilehealth.ui.paswordupdate.model.ChangeModel;

/**
 * Created by ck-nivetha on 13/9/17.
 */

public interface PasswordPresenter extends Presenter<PasswordView> {

    void ChangePassword(ChangeModel changeModel);
}
