package com.agilehealth.ui.paswordupdate.model;

/**
 * Created by ck-nivetha on 13/9/17.
 */

public class ChangeResponse {

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String Result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String message;
}
