package com.agilehealth.ui.QuestionsAnswers;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.agilehealth.R;
import com.agilehealth.base.BaseActivity;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.ui.QuestionsAnswers.adapter.QuestionsAnswersAdapter;
import com.agilehealth.ui.QuestionsAnswers.model.Recycle;
import com.agilehealth.ui.QuestionsAnswers.presenter.QuestionsAnswersPresenter;
import com.agilehealth.ui.QuestionsAnswers.presenter.QuestionsAnswersView;

import java.util.ArrayList;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionsAnswersActivity extends BaseActivity implements QuestionsAnswersView {


    @Inject
    QuestionsAnswersPresenter presenter;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerviewid)
    RecyclerView recyclerviewid;
    ArrayList<Recycle> userList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions_answers);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attach(this);
        setAdapter();
    }

    @Override
    public void isNetworkAvailable(boolean isAvailable) {

    }

    @Override
    public void setUpToolbar() {

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setAdapter()
    {
        for (int i = 1; i <=15; i++)
        {

            Recycle user = new Recycle();
            user.setNumber(i);
            userList.add(user);
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        recyclerviewid.setLayoutManager(layoutManager);

        QuestionsAnswersAdapter userAdapter = new QuestionsAnswersAdapter(this, userList);
        recyclerviewid.setAdapter(userAdapter);
    }
}
