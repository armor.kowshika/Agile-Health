package com.agilehealth.ui.QuestionsAnswers.presenter;

import com.agilehealth.base.Presenter;

/**
 * Created by ck-nivetha on 16/9/17.
 */

public interface QuestionsAnswersPresenter extends Presenter<QuestionsAnswersView> {
}
