package com.agilehealth.ui.QuestionsAnswers.presenter;

import com.agilehealth.base.BasePresenter;
import com.agilehealth.data.api.ApiService;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.injection.module.ApiModule;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by ck-nivetha on 16/9/17.
 */

public class QuestionsAnswersImpl extends BasePresenter<QuestionsAnswersView>implements QuestionsAnswersPresenter {

    @Named(ApiModule.DEFAULT)
    @Inject
    ApiService apiService;

    @Override
    public void attach(QuestionsAnswersView questionsAnswersView) {
        super.attach(questionsAnswersView);
        DaggerAppComponent.create().inject(this);
        getView().setUpToolbar();
    }
}
