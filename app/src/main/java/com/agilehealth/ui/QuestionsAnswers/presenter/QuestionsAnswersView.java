package com.agilehealth.ui.QuestionsAnswers.presenter;

import com.agilehealth.base.BaseView;

/**
 * Created by ck-nivetha on 16/9/17.
 */

public interface QuestionsAnswersView extends BaseView {

    void setAdapter();
}
