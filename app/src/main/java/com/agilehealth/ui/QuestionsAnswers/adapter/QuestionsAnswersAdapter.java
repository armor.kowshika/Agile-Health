package com.agilehealth.ui.QuestionsAnswers.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agilehealth.R;
import com.agilehealth.ui.QuestionsAnswers.QuestionsAnswersActivity;
import com.agilehealth.ui.QuestionsAnswers.model.Recycle;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ck-nivetha on 16/9/17.
 */

public class QuestionsAnswersAdapter extends RecyclerView.Adapter<QuestionsAnswersAdapter.ViewHolder> {


    ArrayList<Recycle> objects;

    public QuestionsAnswersActivity questionsAnswersActivity;

    public QuestionsAnswersAdapter(QuestionsAnswersActivity questionsAnswersActivity, ArrayList<Recycle> objects) {
        this.questionsAnswersActivity=questionsAnswersActivity;
        this.objects=objects;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View convertView;
        convertView = layoutInflater.inflate(R.layout.recycleviewitem, parent, false);
        ViewHolder viewHolder = new ViewHolder(convertView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {

        Recycle recycle;
        recycle = objects.get(position);
        holder.numTxt.setText(String.valueOf(recycle.getNumber()));

    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.num_txt)
        TextView numTxt;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
