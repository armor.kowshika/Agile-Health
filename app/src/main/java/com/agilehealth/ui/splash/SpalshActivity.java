package com.agilehealth.ui.splash;

import android.content.Intent;
import android.os.Bundle;

import com.agilehealth.R;
import com.agilehealth.base.BaseActivity;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.ui.login.LoginActivity;
import com.agilehealth.ui.main.MainActivity;
import com.agilehealth.ui.splash.presenter.SplashPresenter;
import com.agilehealth.ui.splash.presenter.SplashView;
import com.agilehealth.util.toast.ToastMaker;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SpalshActivity extends BaseActivity implements SplashView {

    @Inject
    SplashPresenter Presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        Presenter.attach(this);
    }

    @Override
    public void isNetworkAvailable(boolean isAvailable) {
        if (!isAvailable) {
            ToastMaker.makeToast(this, "No internet connection");
        }
    }

    @Override
    public void setUpToolbar() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void goToHome()
    {
        Intent homePage = new Intent(SpalshActivity.this, LoginActivity.class);
        startActivity(homePage);
    }

    @Override
    public void goToMain() {
        Intent homePage = new Intent(SpalshActivity.this, MainActivity.class);
        startActivity(homePage);
    }

    @Override
    public void goTo()
    {
        Presenter.goTo(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Presenter.onResume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        Presenter.onPause();
    }
}
