package com.agilehealth.ui.splash.presenter;

import android.content.Context;

import com.agilehealth.base.BasePresenter;
import com.agilehealth.base.Presenter;

/**
 * Created by ck-nivetha on 11/9/17.
 */

public interface SplashPresenter extends Presenter<SplashView> {

    void onResume();

    void onPause();

    void goTo(Context applicationContext);

}
