package com.agilehealth.ui.splash.presenter;

import android.content.Context;
import android.os.Handler;

import com.agilehealth.base.BasePresenter;
import com.agilehealth.data.PrefConnect;
import com.agilehealth.injection.component.DaggerAppComponent;

import javax.inject.Inject;

/**
 * Created by ck-nivetha on 11/9/17.
 */

public class SplashImpl extends BasePresenter<SplashView> implements SplashPresenter, Runnable {


    private static final long TIMEOUT = 2000;

    private Handler handler = new Handler();

    Context context;

    @Override
    public void attach(SplashView splashView) {
        super.attach(splashView);
        DaggerAppComponent.create().inject(this);
        getView().goTo();
    }


    @Override
    public void onResume() {

        handler.postDelayed(this, TIMEOUT);
    }

    @Override
    public void onPause() {

        handler.removeCallbacks(this);
    }

    @Override
    public void goTo(Context applicationContext) {
        context = applicationContext;
    }


    @Override
    public void run()
    {
        if (PrefConnect.readBoolean(context,PrefConnect.LOGIN,true) || PrefConnect.readBoolean(context,PrefConnect.REGISTER,true))
        {
            getView().goToMain();
        }else if (PrefConnect.readBoolean(context, PrefConnect.LOGOUT, true))
        {
            getView().goToHome();
        }


    }
}


