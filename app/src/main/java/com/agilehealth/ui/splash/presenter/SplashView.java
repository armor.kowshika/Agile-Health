package com.agilehealth.ui.splash.presenter;

import com.agilehealth.base.BaseView;

/**
 * Created by ck-nivetha on 11/9/17.
 */

public interface SplashView extends BaseView {

    void goToHome();

    void goToMain();

    void goTo();
}
