package com.agilehealth.ui.profilefragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.agilehealth.R;
import com.agilehealth.app.App;
import com.agilehealth.data.Config;
import com.agilehealth.data.dialog.PhotoChooserDialog;
import com.agilehealth.data.global.Global;
import com.agilehealth.data.util.FilePath;
import com.agilehealth.data.util.TitleChangeListener;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.session.Profile;
import com.agilehealth.ui.paswordupdate.ChangePasswordActivity;
import com.agilehealth.ui.profilefragment.model.LinearLayoutTarget;
import com.agilehealth.ui.profilefragment.presenter.ProfilePresenter;
import com.agilehealth.ui.profilefragment.presenter.ProfileView;
import com.agilehealth.ui.main.MainActivity;
import com.agilehealth.util.toast.ToastMaker;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.iceteck.silicompressorr.SiliCompressor;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public class ProfileFragment extends Fragment implements ProfileView, PhotoChooserDialog.ImageCallback, PhotoChooserDialog.Callback {


    @Inject
    ProfilePresenter presenter;
    @BindView(R.id.btnUpdate)
    AppCompatButton btnUpdate;
    @BindView(R.id.profileImage)
    CircleImageView profileImage;
    @BindView(R.id.cameraImg)
    CircleImageView cameraImg;
    @BindView(R.id.profileImageLay)
    FrameLayout profileImageLay;
    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.edtFirstName)
    AppCompatEditText edtFirstName;
    @BindView(R.id.editFirstNameImage)
    ImageView editFirstNameImage;
    @BindView(R.id.relative)
    RelativeLayout relative;
    @BindView(R.id.iconUser)
    ImageView iconUser;
    @BindView(R.id.edtLastName)
    AppCompatEditText edtLastName;
    @BindView(R.id.editLastNameImage)
    ImageView editLastNameImage;
    @BindView(R.id.relativeLast)
    RelativeLayout relativeLast;
    @BindView(R.id.iconUser2)
    ImageView iconUser2;
    @BindView(R.id.edtTitle)
    AppCompatEditText edtTitle;
    @BindView(R.id.editTitleImage)
    ImageView editTitleImage;
    @BindView(R.id.relativeTitle)
    RelativeLayout relativeTitle;
    @BindView(R.id.iconUser3)
    ImageView iconUser3;
    @BindView(R.id.edtCompany)
    AppCompatEditText edtCompany;
    @BindView(R.id.editCompanyImage)
    ImageView editCompanyImage;
    @BindView(R.id.relativeCompany)
    RelativeLayout relativeCompany;
    @BindView(R.id.iconUser4)
    ImageView iconUser4;
    @BindView(R.id.edtCertificates)
    AppCompatEditText edtCertificates;
    @BindView(R.id.agileCertificatesImage)
    ImageView agileCertificatesImage;
    @BindView(R.id.relativeCertificate)
    RelativeLayout relativeCertificate;
    @BindView(R.id.iconUser5)
    ImageView iconUser5;
    @BindView(R.id.edtEmail)
    AppCompatEditText edtEmail;
    @BindView(R.id.editEmailImage)
    ImageView editEmailImage;
    @BindView(R.id.relativeemail)
    RelativeLayout relativeemail;
    @BindView(R.id.iconUser6)
    ImageView iconUser6;
    @BindView(R.id.edtPass)
    AppCompatEditText edtPass;
    @BindView(R.id.editPassword)
    ImageView editPassword;
    @BindView(R.id.relativePassword)
    RelativeLayout relativePassword;
    @BindView(R.id.linarchild)
    LinearLayout linarchild;
    @BindView(R.id.nested)
    NestedScrollView nested;
    @BindView(R.id.linear)
    LinearLayout linear;
    Unbinder unbinder;
    boolean isChanged = false;
    Uri profileURI, cameraURI, fileUri;
    BottomSheetDialog mBottomSheetDialog;
    View bottomSheet;
    public File avatarFile;
    List<Profile> profile;
    Context context;
    @BindView(R.id.linarheader)
    LinearLayout linarheader;

    public ProfileFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        try {
            TitleChangeListener changeListener = (TitleChangeListener) context;
            changeListener.changeTitle(getString(R.string.profile));
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View First = inflater.inflate(R.layout.profilefragment, container, false);
        unbinder = ButterKnife.bind(this, First);
        DaggerAppComponent.create().inject(this);
        presenter.attach(this);

        profile = App.getInstance().profileDao.queryBuilder().list();

        if (profile.size()>0)
        {
            edtFirstName.setText(profile.get(0).getFirstName());

            Log.e("name ", " " + profile.get(0).getFirstName());
            edtLastName.setText(profile.get(0).getLastName());
            edtEmail.setText(profile.get(0).getEmail());
            edtPass.setText(profile.get(0).getPassword());
            edtCompany.setText(profile.get(0).getCompany());
            edtTitle.setText(profile.get(0).getTitle());
            edtCertificates.setText(profile.get(0).getAgileCertification());
            Log.e("password", " " + profile.get(0).getPassword());

            Glide.with(this.getActivity())
                    .load(Config.IMAGE_BASE_URL + profile.get(0).getProfilePicture())
                    .asBitmap()
                    .into(new LinearLayoutTarget(this.getActivity(), linarheader));


            Glide.with(this)
                    .load(Config.IMAGE_BASE_URL + profile.get(0).getProfilePicture())
                    .error(R.drawable.avatar)
                    .into(profileImage);

            Log.e("tag", "photo" + Config.IMAGE_BASE_URL + profile.get(0).getProfilePicture());
        }

       /* Glide.with(this).
                load(Config.IMAGE_BASE_URL + profile.get(0).getProfilePicture())
                .error(R.drawable.avatar)
                .into(profileImage);

*/
       /* edtFirstName.setText(PrefConnect.readString(getActivity(), PrefConnect.FIRST_NAME, ""));

        edtLastName.setText(PrefConnect.readString(getActivity(), PrefConnect.LAST_NAME, ""));


        edtEmail.setText(PrefConnect.readString(getActivity(), PrefConnect.EMAIL, ""));

        edtPass.setText(PrefConnect.readString(getActivity(), PrefConnect.PASSWORD, ""));
*/
        return First;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @OnClick({R.id.editFirstNameImage, R.id.profileImage, R.id.btnUpdate, R.id.cameraImg, R.id.editLastNameImage, R.id.relativePassword, R.id.editTitleImage, R.id.editCompanyImage, R.id.agileCertificatesImage, R.id.editEmailImage, R.id.editPassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.editFirstNameImage:
                editFirstNameImage.setImageResource(R.drawable.close);
                edtFirstName.setFocusableInTouchMode(true);
                break;

            case R.id.editLastNameImage:
                editLastNameImage.setImageResource(R.drawable.close);
                edtLastName.setFocusableInTouchMode(true);
                break;
            case R.id.editTitleImage:
                editTitleImage.setImageResource(R.drawable.close);
                edtTitle.setFocusableInTouchMode(true);
                break;
            case R.id.editCompanyImage:
                editCompanyImage.setImageResource(R.drawable.close);
                edtCompany.setFocusableInTouchMode(true);
                break;
            case R.id.agileCertificatesImage:
                agileCertificatesImage.setImageResource(R.drawable.close);
                edtCertificates.setFocusableInTouchMode(true);
                break;
            case R.id.editEmailImage:
                editEmailImage.setImageResource(R.drawable.close);
                if (editEmailImage.getDrawable() == getResources().getDrawable(R.drawable.close)) {
                    edtEmail.setText(" ");
                    editEmailImage.setImageResource(R.drawable.edit);
                }
                edtEmail.setFocusableInTouchMode(true);
                break;
            case R.id.editPassword:
                editPassword.setImageResource(R.drawable.close);
                edtPass.setFocusableInTouchMode(true);
                break;
            case R.id.relativePassword:
                Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.profileImage:
                if (isCameraPermissionEnabled() && isStoragePermissionEnabled()) {
                    presenter.openPhotoChooser();
                } else {
                    checkPermissions();
                }
                break;
            case R.id.cameraImg:
                if (isCameraPermissionEnabled() && isStoragePermissionEnabled()) {
                    presenter.openPhotoChooser();
                } else {
                    checkPermissions();
                }
                break;
            case R.id.btnUpdate:
                HashMap<String, RequestBody> updateMap = new HashMap();
                HashMap<String, MultipartBody.Part> imageMap = new HashMap();
                updateMap.put("email", createPartFromString(edtEmail.getText().toString()));
                updateMap.put("mobile", createPartFromString(" "));
                updateMap.put("firstName", createPartFromString(edtFirstName.getText().toString()));
                updateMap.put("lastName", createPartFromString(edtLastName.getText().toString()));
                updateMap.put("title", createPartFromString(edtTitle.getText().toString()));
                updateMap.put("agileCertification", createPartFromString(edtCertificates.getText().toString()));
                updateMap.put("company", createPartFromString(edtCompany.getText().toString()));
                if(profile.size()>0) {
                    updateMap.put("id", createPartFromString(String.valueOf(profile.get(0).getId())));
                }
                if (profileURI != null) {

                    avatarFile = FilePath.getPath(getActivity(), profileURI);
                }


                if (avatarFile != null) {
                    RequestBody requestFile =
                            RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
                    MultipartBody.Part body =
                            MultipartBody.Part.createFormData(
                                    "image", avatarFile.getName(), requestFile);
                    imageMap.put("image", body);
                    Log.e("body"," "+body);
                    Log.e("Profile __", String.valueOf(updateMap));
                }
                else
                {
                  //  avatarFile = FilePath.getPath(getActivity(), profileURI);

                    RequestBody requestFile =
                            RequestBody.create(MediaType.parse("multipart/form-data"), profile.get(0).getProfilePicture());
                    MultipartBody.Part body =
                            MultipartBody.Part.createFormData(
                                    "image", profile.get(0).getProfilePicture(), requestFile);

                    Log.e("image"," show"+ profile.get(0).getProfilePicture());
                    imageMap.put("image", body);


                }

                presenter.updateProfile(updateMap, imageMap.get("image"));
                break;
        }
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        String assign = descriptionString;
        Log.e("assign", assign);

        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setUpToolbar() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }


    @Override
    public void goToSucess() {
        ToastMaker.makeToast(getActivity(), "Prfoile updated successfully");
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onImageReceived(Uri imageUri) {
        String filePath = SiliCompressor.with(getActivity()).compress(imageUri.toString(), true);
        File file = new File(filePath);
        profileURI = Uri.fromFile(file);
        Glide.with(this).load(profileURI).error(R.drawable.avatar).into(profileImage);
        isChanged = true;
    }

    @Override
    public void removeImage() {
        Glide.with(this)
                .load(R.drawable.avatar)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(profileImage);
        isChanged = true;
    }

    @Override
    public boolean isCameraPermissionEnabled() {
        return ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public boolean isStoragePermissionEnabled() {
        return ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void showDialog() {

    }


    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            ArrayList<String> permissionList = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                permissionList.add(Manifest.permission.CAMERA);
            }

            if (permissionList.size() > 0) {
                String[] permissionArray = new String[permissionList.size()];
                permissionList.toArray(permissionArray);
                requestPermissions(permissionArray, Global.PERMISSION_REQUEST);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

}


