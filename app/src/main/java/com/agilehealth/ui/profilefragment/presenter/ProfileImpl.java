package com.agilehealth.ui.profilefragment.presenter;

import android.net.Uri;

import com.agilehealth.app.App;
import com.agilehealth.base.BasePresenter;
import com.agilehealth.data.api.ApiService;
import com.agilehealth.data.dialog.PhotoChooserDialog;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.injection.module.ApiModule;
import com.agilehealth.session.Profile;
import com.agilehealth.ui.profilefragment.model.UpdateResponse;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ck-nivetha on 13/9/17.
 */

public class ProfileImpl extends BasePresenter<ProfileView>implements ProfilePresenter {

    @Named(ApiModule.DEFAULT)
    @Inject
    ApiService apiService;



    @Override
    public void attach(ProfileView profileView) {
        super.attach(profileView);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void openPhotoChooser()
    {
        PhotoChooserDialog chooserDialog = new PhotoChooserDialog();

        chooserDialog.setImageCallback(
                new PhotoChooserDialog.ImageCallback() {
                    @Override
                    public void onImageReceived(Uri imageUri) {
                        getView().onImageReceived(imageUri);
                    }
                });


        chooserDialog.setCallback(new PhotoChooserDialog.Callback()
        {
            @Override
            public void removeImage()
            {
                getView().removeImage();
            }
        });

        chooserDialog.show(getView().getChildFragmentManager(), "PhotoChooserDialog");
    }

    @Override
    public void updateProfile(final HashMap<String, RequestBody> updateMap, MultipartBody.Part profilePhoto) {

        Call<UpdateResponse>call=apiService.updateProfile(updateMap,profilePhoto);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                if (response.isSuccessful()) {
                    UpdateResponse updateResponse = response.body();
                    List<Profile> profiles = App.getInstance().profileDao.queryBuilder().list();
                    if (profiles.size() > 0) {
                        Profile p = profiles.get(0);
                        p.setCompany(updateResponse.getDetails().getCompany());
                        p.setTitle(updateResponse.getDetails().getTitle());
                        p.setProfilePicture(updateResponse.getDetails().getProfilePicture());
                        App.getInstance().profileDao.update(p);
                        getView().goToSucess();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {

            }
        });
    }
}
