package com.agilehealth.ui.profilefragment.presenter;

import com.agilehealth.base.Presenter;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by ck-nivetha on 13/9/17.
 */

public interface ProfilePresenter extends Presenter<ProfileView> {

    void openPhotoChooser();

    void updateProfile(HashMap<String, RequestBody> updateMap, MultipartBody.Part profilePhoto);


}
