package com.agilehealth.ui.profilefragment.presenter;

import android.net.Uri;
import android.support.v4.app.FragmentManager;

import com.agilehealth.base.BaseView;

/**
 * Created by ck-nivetha on 13/9/17.
 */

public interface ProfileView extends BaseView {

    void goToSucess();

    void onImageReceived(Uri imageUri);

    void removeImage();

    FragmentManager getChildFragmentManager();


    boolean isCameraPermissionEnabled();

    boolean isStoragePermissionEnabled();

    void showDialog();


}
