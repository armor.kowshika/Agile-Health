package com.agilehealth.ui.profilefragment.model;

/**
 * Created by ck-nivetha on 14/9/17.
 */

public class UpdateResponse
{

    public String Result;

    public String Status;
public Update details;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public Update getDetails() {
        return details;
    }

    public void setDetails(Update details) {
        this.details = details;
    }

    public class Update
    {
        public String id;
        public String firstName;
        public String lastName;
        public String email;
        public String profilePicture;
        public String mobile;
        public String password;
        public String title;
        public String company;
        public String agileCertification;
        public String authToken;
        public String tempToken;
        public String profileStatus;
        public String isLogin;
        public String deviceType;
        public String deviceToken;
        public String environment;
        public String created_at;
        public String updated_at;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProfilePicture() {
            return profilePicture;
        }

        public void setProfilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getAgileCertification() {
            return agileCertification;
        }

        public void setAgileCertification(String agileCertification) {
            this.agileCertification = agileCertification;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public String getTempToken() {
            return tempToken;
        }

        public void setTempToken(String tempToken) {
            this.tempToken = tempToken;
        }

        public String getProfileStatus() {
            return profileStatus;
        }

        public void setProfileStatus(String profileStatus) {
            this.profileStatus = profileStatus;
        }

        public String getIsLogin() {
            return isLogin;
        }

        public void setIsLogin(String isLogin) {
            this.isLogin = isLogin;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getEnvironment() {
            return environment;
        }

        public void setEnvironment(String environment) {
            this.environment = environment;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }

}
