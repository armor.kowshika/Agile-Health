package com.agilehealth.ui.share;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agilehealth.R;
import com.agilehealth.data.util.TitleChangeListener;

public class ShareFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View First = inflater.inflate(R.layout.dashboard_activity, container, false);


        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>Agile Health - Shared.</p>"));
        startActivity(Intent.createChooser(sharingIntent,"Share using"));

        return First;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            TitleChangeListener changeListener = (TitleChangeListener) context;
            changeListener.changeTitle(getString(R.string.share));
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }
}
