package com.agilehealth.ui.main.model;

/**
 * Created by ck-nivetha on 12/9/17.
 */

public class LogoutResponse {


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String message;
}
