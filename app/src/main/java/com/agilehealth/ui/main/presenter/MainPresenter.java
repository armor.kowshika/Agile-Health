package com.agilehealth.ui.main.presenter;

import com.agilehealth.base.Presenter;

/**
 * Created by ck-nivetha on 12/9/17.
 */

public interface MainPresenter extends Presenter<MainView> {

    void logout(String id);
}
