package com.agilehealth.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.agilehealth.R;
import com.agilehealth.app.App;
import com.agilehealth.base.BaseActivity;
import com.agilehealth.data.PrefConnect;
import com.agilehealth.data.util.ProgressUtils;
import com.agilehealth.data.util.TitleChangeListener;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.session.DaoSession;
import com.agilehealth.session.Profile;
import com.agilehealth.ui.dashboard_fragment.DashboardFragment;
import com.agilehealth.ui.login.LoginActivity;
import com.agilehealth.ui.profilefragment.ProfileFragment;
import com.agilehealth.ui.share.ShareFragment;
import com.agilehealth.ui.main.presenter.MainPresenter;
import com.agilehealth.ui.main.presenter.MainView;

import com.agilehealth.ui.supportfragment.SupportFragement;
import com.agilehealth.util.toast.ToastMaker;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainView, TitleChangeListener {

    @Inject
    MainPresenter presenter;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.frame)
    FrameLayout frame;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawer)
    DrawerLayout drawer;
   /* @BindView(R.id.profile_image)
    CircleImageView profile_image;
    @BindView(R.id.txtName)
    AppCompatTextView txtName;
    @BindView(R.id.txtSecondName)
    AppCompatTextView txtSecondName;
    @BindView(R.id.txtEmail)
    AppCompatTextView txtEmail;*/
    android.support.v4.app.FragmentManager fragmentManager;


    private ProgressUtils progressUtils;
    String id;
    DaoSession daoSession;
    List<Profile> profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigationdrawer);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        presenter.attach(this);
        progressUtils = new ProgressUtils(this);
        initNavigationDrawer();
        profile = App.getInstance().profileDao.queryBuilder().list();

      /*  txtName.setText(profile.get(0).getFirstName() + " " + profile.get(0).getLastName());
        txtSecondName.setText(profile.get(0).getTitle());
        txtEmail.setText(profile.get(0).getEmail());
        Glide.with(this)
                .load(Config.IMAGE_BASE_URL + profile.get(0).getProfilePicture())
                .error(R.drawable.avatar)
                .into(profile_image);

*/
        //setSupportActionBar(toolbar);
    }

    @Override
    public void isNetworkAvailable(boolean isAvailable) {
        if (!isAvailable) {
            ToastMaker.makeToast(this, "No internet connection");
        }
    }

    private void initNavigationDrawer() {
        fragmentManager = getSupportFragmentManager();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();

                ProfileFragment profileFragment = new ProfileFragment();
                SupportFragement supportFragement = new SupportFragement();
                DashboardFragment dashboardFragment=new DashboardFragment();
                ShareFragment shareFragment=new ShareFragment();

                switch (id) {
                    case R.id.dashBoard:
                        fragmentManager.beginTransaction().replace(R.id.frame, dashboardFragment).commit();
                        drawer.closeDrawers();

                        break;
                    case R.id.profle:
                        fragmentManager.beginTransaction().replace(R.id.frame, profileFragment).commit();
                        drawer.closeDrawers();
                        //  toolbar.setTitle("My Profile");

                        break;
                    case R.id.support:
                        fragmentManager.beginTransaction().replace(R.id.frame, supportFragement).commit();
                        drawer.closeDrawers();
                        //  toolbar.setTitle("Support");
                        break;

                    case R.id.share:
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/html");
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>Agile Health - Shared.</p>"));
                        startActivity(Intent.createChooser(sharingIntent,"Share using"));
                        fragmentManager.beginTransaction().replace(R.id.frame, shareFragment).commit();
                        drawer.closeDrawers();
                        break;
                    case R.id.upGrade:

                        break;
                    case R.id.logout:

                        //List<Profile> profile = App.getInstance().profileDao.queryBuilder().list();
                    /*   if(profile.size() >0)
                       {
                           presenter.logout(String.valueOf(profile.get(0).getId()));
                       }
                       else
                       {
                           Log.e("empty"," ");
                       }*/
                       /* {
                            Profile p =profile.get(0);
                           // p.setCompany();
                        }*/
                     presenter.logout(String.valueOf(id));


                        break;
                }


                return true;
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void setUpToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logo:
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress()

    {
        progressUtils.show();
    }

    @Override
    public void hideProgress()

    {
        progressUtils.dismiss();
    }

    @Override
    public void Sucess()

    {
        PrefConnect.writeBoolean(MainActivity.this, PrefConnect.LOGOUT, true);
        App.getInstance().profileDao.deleteAll();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        ToastMaker.makeToast(this, "Logout Sucessfull");
    }

    @Override
    public void failure(String message) {
        ToastMaker.makeToast(this,message);
    }


    @Override
    public void changeTitle(String title) {
        toolbar.setTitle(title);

    }
}



