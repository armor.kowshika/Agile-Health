package com.agilehealth.ui.main.model;

/**
 * Created by ck-nivetha on 12/9/17.
 */

public class LogoutRequest {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String id;
}
