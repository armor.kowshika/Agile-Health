package com.agilehealth.ui.main.presenter;

import com.agilehealth.base.BasePresenter;
import com.agilehealth.data.api.ApiService;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.injection.module.ApiModule;
import com.agilehealth.ui.main.model.LogoutResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ck-nivetha on 12/9/17.
 */

public class MainImpl extends BasePresenter<MainView>implements MainPresenter {
    @Named(ApiModule.AUTH)
    @Inject
    ApiService apiService;



    @Override
    public void attach(MainView mainView)
    {
        super.attach(mainView);
        DaggerAppComponent.create().inject(this);
        getView().setUpToolbar();
    }

    @Override
    public void logout(final String id) {
        getView().showProgress();

        Call<LogoutResponse>call=apiService.logout(id);
        call.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response)
            {
                getView().hideProgress();
                if(response.isSuccessful())
                {
                    if (response.body().getMessage().equalsIgnoreCase("Customer not found")){
                        getView().failure(response.body().getMessage());
                    }
                      getView().Sucess();
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {

            }
        });
    }
}
