package com.agilehealth.ui.supportfragment.presenter;

import android.util.Log;

import com.agilehealth.base.BasePresenter;
import com.agilehealth.data.api.ApiService;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.injection.module.ApiModule;
import com.agilehealth.ui.supportfragment.model.SupportModel;
import com.agilehealth.ui.supportfragment.model.SupportResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ck-nivetha on 11/9/17.
 */

public class SupportImpl extends BasePresenter<SupportView> implements SupportPresenter {

    @Named(ApiModule.DEFAULT)
    @Inject
    ApiService apiService;


    @Override
    public void attach(SupportView supportView)
    {
        super.attach(supportView);
        DaggerAppComponent.create().inject(this);
    }

    @Override
    public void Support(SupportModel supportModel)
    {
        getView().showProgress();
        Call<SupportResponse>call=apiService.Support(supportModel);
        Log.e("response","sucess" + call);
        call.enqueue(new Callback<SupportResponse>()
        {
            @Override
            public void onResponse(Call<SupportResponse> call, Response<SupportResponse> response)
            {
                getView().hideProgress();
                Log.e("response","sucess" + call);
                if(response.isSuccessful())
                {
                    if(response.body().getStatus().equals("Email sent failed"))
                    {
                        getView().Sucess("Mail Not Send Sucessfully");

                    }
                    else if(response.body().getStatus().equals("Email sent Sucessfully"))
                    {
                        getView().Sucess("Mail Send Sucessfully");
                    }

                }
            }

            @Override
            public void onFailure(Call<SupportResponse> call, Throwable t)
            {
                Log.e("response","sucess" + t);

            }
        });
    }
}
