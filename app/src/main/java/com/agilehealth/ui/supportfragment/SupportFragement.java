package com.agilehealth.ui.supportfragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.agilehealth.R;
import com.agilehealth.data.util.ProgressUtils;
import com.agilehealth.data.util.TitleChangeListener;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.ui.QuestionsAnswers.QuestionsAnswersActivity;
import com.agilehealth.ui.supportfragment.model.SupportModel;
import com.agilehealth.ui.supportfragment.presenter.SupportPresenter;
import com.agilehealth.ui.supportfragment.presenter.SupportView;
import com.agilehealth.util.toast.ToastMaker;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SupportFragement extends Fragment implements SupportView {

    @Inject
    SupportPresenter presenter;
    @BindView(R.id.nameText)
    AppCompatTextView nameText;
    @BindView(R.id.edtUserName)
    AppCompatEditText edtUserName;
    @BindView(R.id.relative)
    RelativeLayout relative;
    @BindView(R.id.email)
    AppCompatTextView email;
    @BindView(R.id.edtEmail)
    AppCompatEditText edtEmail;
    @BindView(R.id.relativeEmail)
    RelativeLayout relativeEmail;
    @BindView(R.id.message)
    AppCompatTextView message;
    @BindView(R.id.edtMessage)
    AppCompatEditText edtMessage;
    @BindView(R.id.relativeMessage)
    RelativeLayout relativeMessage;
    @BindView(R.id.submit)
    AppCompatTextView submit;
    @BindView(R.id.contact)
    AppCompatTextView contact;
    Unbinder unbinder;
    @BindView(R.id.clear)
    AppCompatTextView clear;
    @BindView(R.id.sample)
    AppCompatTextView sample;
    private ProgressUtils progressUtils;

    public SupportFragement() {

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View First = inflater.inflate(R.layout.activity_support_fragement, container, false);
        unbinder = ButterKnife.bind(this, First);
        DaggerAppComponent.create().inject(this);
        presenter.attach(this);
     /*   sample.setText(Html.fromHtml(getString(R.string.one_question_product)));
        sample.setMovementMethod(LinkMovementMethod.getInstance());*/
        progressUtils = new ProgressUtils(getActivity());


        return First;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            TitleChangeListener changeListener = (TitleChangeListener) context;
            changeListener.changeTitle(getString(R.string.support));
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUpToolbar() {

    }

    @Override
    public void showProgress() {
        progressUtils.show();
    }

    @Override
    public void hideProgress() {
        progressUtils.dismiss();
    }

    @OnClick({R.id.submit,R.id.clear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.submit:
                SupportModel model = new SupportModel();
                model.setName(edtUserName.getText().toString());
                model.setEmail(edtEmail.getText().toString());
                model.setMessage(edtMessage.getText().toString());
                presenter.Support(model);
                break;
            case R.id.clear:
                Intent intent=new Intent(getActivity(),QuestionsAnswersActivity.class);
                startActivity(intent);

        }
    }


    @Override
    public void Sucess(String sucess) {
        ToastMaker.makeToast(getActivity(), sucess);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
