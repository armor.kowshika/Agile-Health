package com.agilehealth.ui.supportfragment.model;

/**
 * Created by ck-nivetha on 14/9/17.
 */

public class SupportResponse {
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String Status;
    public String Result;
}
