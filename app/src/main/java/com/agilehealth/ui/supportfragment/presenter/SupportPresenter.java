package com.agilehealth.ui.supportfragment.presenter;

import com.agilehealth.base.Presenter;
import com.agilehealth.ui.supportfragment.model.SupportModel;

/**
 * Created by ck-nivetha on 11/9/17.
 */

public interface SupportPresenter extends Presenter<SupportView> {

    void Support(SupportModel supportModel);
}
