package com.agilehealth.ui.dashboard_fragment.model;

import java.util.ArrayList;

/**
 * Created by priya on 19/9/17.
 */

public class MessageDetails {
    String messageId;
    String agileCertification;
    String msgcount;
    String cmdcount;
    String userId;
    String useroption;
    String created_at;
    String estTime;
    String message;
    String totalcount;
    String likeCount;
    String unlikeCount;
    String status;
    String firstName;
    String lastName;
    String email;
    String profilePicture;
    String mobile;
    String title;
    String company;
    String commentId;
    String commentdUserOption;
    public ArrayList<Commemds> commands;

    int viewType;
    public Commemds commemd;
    boolean expand;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getAgileCertification() {
        return agileCertification;
    }

    public void setAgileCertification(String agileCertification) {
        this.agileCertification = agileCertification;
    }

    public String getMsgcount() {
        return msgcount;
    }

    public void setMsgcount(String msgcount) {
        this.msgcount = msgcount;
    }

    public String getCmdcount() {
        return cmdcount;
    }

    public void setCmdcount(String cmdcount) {
        this.cmdcount = cmdcount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUseroption() {
        return useroption;
    }

    public void setUseroption(String useroption) {
        this.useroption = useroption;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getEstTime() {
        return estTime;
    }

    public void setEstTime(String estTime) {
        this.estTime = estTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(String totalcount) {
        this.totalcount = totalcount;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getUnlikeCount() {
        return unlikeCount;
    }

    public void setUnlikeCount(String unlikeCount) {
        this.unlikeCount = unlikeCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public ArrayList<Commemds> getCommands() {
        return commands;
    }

    public void setCommands(ArrayList<Commemds> commands) {
        this.commands = commands;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public Commemds getCommemd() {
        return commemd;
    }

    public void setCommemd(Commemds commemd) {
        this.commemd = commemd;
    }

    public boolean isExpand() {
        return expand;
    }

    public void setExpand(boolean expand) {
        this.expand = expand;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getCommentdUserOption() {
        return commentdUserOption;
    }

    public void setCommentdUserOption(String commentdUserOption) {
        this.commentdUserOption = commentdUserOption;
    }
}
