package com.agilehealth.ui.dashboard_fragment.presenter;

import android.os.Bundle;

import com.agilehealth.base.BaseView;
import com.agilehealth.ui.dashboard_fragment.model.Dashboardmodel;
import com.agilehealth.ui.dashboard_fragment.model.MostPopularResponse;
import com.agilehealth.ui.dashboard_fragment.model.PostResponse;

import java.util.ArrayList;

/**
 * Created by ck-kowshika on 14/9/17.
 */

public interface DashboardView extends BaseView {

   void setAdapter(ArrayList<Dashboardmodel> arralist);
    void initRecyclerView();
    void mostPopularResponse(MostPopularResponse mostPopularResponse);
    void mostRecentResponse(MostPopularResponse mostPopularResponse);
    void getPostResponse();
    void getLikeUpdate();
    void toExpand(MostPopularResponse args);
}
