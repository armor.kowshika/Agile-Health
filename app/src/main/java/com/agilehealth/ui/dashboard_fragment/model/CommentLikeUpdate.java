package com.agilehealth.ui.dashboard_fragment.model;

/**
 * Created by priya on 22/9/17.
 */

public class CommentLikeUpdate {
    private String messageId;
    private String type;
    private Long userId;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}

