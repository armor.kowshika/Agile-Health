package com.agilehealth.ui.dashboard_fragment.model;

/**
 * Created by priya on 19/9/17.
 */

public class Commemds {
    String firstName;
    String lastName;
    String agileCertification;
    String email;
    String profilePicture;
    String mobile;
    String title;
    String company;
    String commentId;
    String comment;
    String likeCount;
    String unlikeCount;
    String created_at;
    String estTime;
    String reply;
    String msgcounts;
    String cmdcounts;
    String useroption;
    String replyName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAgileCertification() {
        return agileCertification;
    }

    public void setAgileCertification(String agileCertification) {
        this.agileCertification = agileCertification;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getUnlikeCount() {
        return unlikeCount;
    }

    public void setUnlikeCount(String unlikeCount) {
        this.unlikeCount = unlikeCount;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getEstTime() {
        return estTime;
    }

    public void setEstTime(String estTime) {
        this.estTime = estTime;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getMsgcounts() {
        return msgcounts;
    }

    public void setMsgcounts(String msgcounts) {
        this.msgcounts = msgcounts;
    }

    public String getCmdcounts() {
        return cmdcounts;
    }

    public void setCmdcounts(String cmdcounts) {
        this.cmdcounts = cmdcounts;
    }

    public String getUseroption() {
        return useroption;
    }

    public void setUseroption(String useroption) {
        this.useroption = useroption;
    }

    public String getReplyName() {
        return replyName;
    }

    public void setReplyName(String replyName) {
        this.replyName = replyName;
    }


}
