package com.agilehealth.ui.dashboard_fragment.adapter;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agilehealth.R;
import com.agilehealth.data.Config;
import com.agilehealth.data.global.Global;
import com.agilehealth.ui.dashboard_fragment.DashboardFragment;
import com.agilehealth.ui.dashboard_fragment.model.Commemds;
import com.agilehealth.ui.dashboard_fragment.model.MessageDetails;
import com.agilehealth.ui.dashboard_fragment.model.MostPopularResponse;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by kowsi on 20/9/17.
 */

public class DashboardParkingLotAdapter extends RecyclerView.Adapter<DashboardParkingLotAdapter.ViewHolder> {
    private final DashboardFragment activity;
    //private final MostPopularResponse arraylist;
    List<MessageDetails> messageDetails = new ArrayList<>();
    //    List<MessageDetails> messageDetailsClone = new ArrayList<>();
    DashboardCallback dashboardCallback;
    int like;
    String search = "";


    int parkinglot = 0, answers = 1;



    public void setDashboardCallback(DashboardCallback dashboardCallback) {
        this.dashboardCallback = dashboardCallback;
    }

    public DashboardParkingLotAdapter(DashboardFragment activity, MostPopularResponse arraylist, MostPopularResponse arraylistclone) {
        this.activity = activity;
        //this.arraylist = arraylist;
        // messageDetails = arraylist.getMessageDetails();
        MessageDetails[] messageDetailsArray = arraylistclone.getMessageDetails().toArray(new MessageDetails[0]);
        for (MessageDetails messageDetail : messageDetailsArray) {
            MessageDetails m = new MessageDetails();
            m.setCommands(messageDetail.commands);
            m.setFirstName(messageDetail.getFirstName());
            m.setLastName(messageDetail.getLastName());
            m.setProfilePicture(messageDetail.getProfilePicture());
            m.setMessage(messageDetail.getMessage());
            m.setEstTime(messageDetail.getEstTime());
            m.setLikeCount(messageDetail.getLikeCount());
            m.setMessageId(messageDetail.getMessageId());
            m.setUseroption(messageDetail.getUseroption());
            m.setTotalcount(messageDetail.getTotalcount());
            messageDetails.add(m);
        }
    }

    public void refresh(MostPopularResponse arraylist) {
        System.out.println("lol");
        messageDetails = new ArrayList<>();
        MessageDetails[] messageDetailsArray = arraylist.getMessageDetails().toArray(new MessageDetails[0]);
        for (MessageDetails messageDetail : messageDetailsArray) {
            MessageDetails m = new MessageDetails();
            m.setCommands(messageDetail.commands);
            m.setFirstName(messageDetail.getFirstName());
            m.setLastName(messageDetail.getLastName());
            m.setProfilePicture(messageDetail.getProfilePicture());
            m.setMessage(messageDetail.getMessage());
            m.setEstTime(messageDetail.getEstTime());
            m.setLikeCount(messageDetail.getLikeCount());
            m.setMessageId(messageDetail.getMessageId());
            m.setUseroption(messageDetail.getUseroption());
            m.setTotalcount(messageDetail.getTotalcount());
            messageDetails.add(m);
            // TODO
            /*
               for (Commemds command : messageDetail.commands) {
                                MessageDetails m = new MessageDetails();
                                m.setViewType(answers);
                                m.setFirstName(command.getFirstName());
                                m.setLastName(command.getLastName());
                                m.setProfilePicture(command.getProfilePicture());
                                m.setMessage(command.getComment());
                                m.setEstTime(command.getEstTime());
                                m.setLikeCount(command.getLikeCount());
                                m.setCommentId(command.getCommentId());
                                m.setCommentdUserOption(command.getUseroption());
                                m.setUseroption(command.getUseroption());
                                m.setCommemd(command);
                                messageDetails.add(c++, m);
                            }
            */
        }
        notifyDataSetChanged();
    }


    public void expand(MostPopularResponse arraylist){

        System.out.println("lol");
        messageDetails = new ArrayList<>();
        MessageDetails[] messageDetailsArray = arraylist.getMessageDetails().toArray(new MessageDetails[0]);
        for (MessageDetails messageDetail : messageDetailsArray) {
            MessageDetails m = new MessageDetails();
            m.setCommands(messageDetail.commands);
            m.setFirstName(messageDetail.getFirstName());
            m.setLastName(messageDetail.getLastName());
            m.setProfilePicture(messageDetail.getProfilePicture());
            m.setMessage(messageDetail.getMessage());
            m.setEstTime(messageDetail.getEstTime());
            m.setLikeCount(messageDetail.getLikeCount());
            m.setMessageId(messageDetail.getMessageId());
            m.setUseroption(messageDetail.getUseroption());
            m.setTotalcount(messageDetail.getTotalcount());
            messageDetails.add(m);
            // TODO
                int c = 0 + 1;
               for (Commemds command : messageDetail.commands) {
                                MessageDetails msg = new MessageDetails();
                                msg.setViewType(answers);
                                msg.setFirstName(command.getFirstName());
                                msg.setLastName(command.getLastName());
                                msg.setProfilePicture(command.getProfilePicture());
                                msg.setMessage(command.getComment());
                                msg.setEstTime(command.getEstTime());
                                msg.setLikeCount(command.getLikeCount());
                                msg.setCommentId(command.getCommentId());
                                msg.setCommentdUserOption(command.getUseroption());
                                msg.setUseroption(command.getUseroption());
                                msg.setCommemd(command);
                                messageDetails.add(c++, msg);
                            }

        }
        notifyDataSetChanged();
    }

    public void collapse(){
        if (dashboardCallback != null) {
            messageDetails = new ArrayList<>();
            notifyDataSetChanged();
            dashboardCallback.dashboardCallback(0);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == parkinglot) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.parking_lot_adapter, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.parking_lot_adapter_answers, parent, false);
        }
        return new ViewHolder(view);
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final MessageDetails messageDetail = messageDetails.get(position);
        if (messageDetail.getViewType() == parkinglot) {
            Glide.with(activity)
                    .load(Config.IMAGE_BASE_URL + messageDetails.get(position).getProfilePicture())
                    .error(R.drawable.avatar)
                    .into(holder.circleImageView);
            holder.txtFirstName.setText(messageDetails.get(position).getFirstName() + " " + messageDetails.get(position).getLastName());
            holder.txtPost.setText(messageDetails.get(position).getMessage());
            holder.txtDateTime.setText(Global.formatedDate(messageDetails.get(position).getEstTime()));
            holder.txtNum.setText(messageDetails.get(position).getLikeCount());
            if (messageDetail.commands != null) {
                if (messageDetail.getCommands().size() > 0) {
                    holder.txtAnswers.setText(String.valueOf(messageDetails.get(position).getCommands().size()) + " answers");
                }
            }
            if (holder.txtAnswers != null) {
                holder.txtAnswers.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean expand = messageDetail.isExpand();
                        messageDetail.setExpand(!expand);
                        //messageDetails.set(position, messageDetail);

                        Log.e("Expand", " " + expand);

                        if (!expand) {
                            int c = position + 1;
                            for (Commemds command : messageDetail.commands) {
                                MessageDetails m = new MessageDetails();
                                m.setViewType(answers);
                                m.setFirstName(command.getFirstName());
                                m.setLastName(command.getLastName());
                                m.setProfilePicture(command.getProfilePicture());
                                m.setMessage(command.getComment());
                                m.setEstTime(command.getEstTime());
                                m.setLikeCount(command.getLikeCount());
                                m.setCommentId(command.getCommentId());
                                m.setCommentdUserOption(command.getUseroption());
                                m.setUseroption(command.getUseroption());
                                m.setCommemd(command);
                                messageDetails.add(c++, m);
                            }
                            notifyDataSetChanged();
                        } else {
                            //messageDetails = messageDetailsClone;
                            if (dashboardCallback != null) {
                                messageDetails = new ArrayList<>();
                                notifyDataSetChanged();
                                dashboardCallback.dashboardCallback(0);
                            }
                        }
//                        Log.e("messageDetails", " " + messageDetails.size() + ", " + messageDetailsClone.size());

//                        notifyDataSetChanged();
                    }
                });
            }

//            if(like==1){

//            }

            holder.imgLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.imgLike.isChecked()) {
                        holder.imgDislike.setChecked(false);
                    }
                    activity.callService(1, messageDetails.get(position).getMessageId());
                }
            });


            holder.imgDislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.imgDislike.isChecked()) {
                        holder.imgLike.setChecked(false);
                    }
                    activity.callService(2, messageDetails.get(position).getMessageId());
                }
            });

            holder.btnReply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.callReply();
                }
            });


            if (messageDetails.get(position).getUseroption().equals("0")) {
                holder.imgLike.setChecked(false);
                holder.imgDislike.setChecked(false);
            } else if (messageDetails.get(position).getUseroption().equals("1")) {
                holder.imgLike.setChecked(true);
            } else if (messageDetails.get(position).getUseroption().equals("2")) {
                holder.imgDislike.setChecked(true);
            }

            String country = messageDetails.get(position).getMessage().toLowerCase(Locale.getDefault());
            if (country.contains(search)) {
                Log.e("test", country + " contains: " + search);
                int startPos = country.indexOf(search);
                int endPos = startPos + search.length();

                Spannable spanText = Spannable.Factory.getInstance().newSpannable(holder.txtPost.getText()); // <- EDITED: Use the original string, as `country` has been converted to lowercase.
                spanText.setSpan(new ForegroundColorSpan(Color.BLUE), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                holder.txtPost.setText(spanText, TextView.BufferType.SPANNABLE);
            }

            activity.vieMore(messageDetails.get(position).getTotalcount());

            if (messageDetails.get(position).getTotalcount()!=null){
                Log.e("Int"," "+Integer.parseInt(messageDetails.get(position).getTotalcount()));
                /*if (Integer.parseInt(messageDetails.get(position).getTotalcount())<20){
                    holder.btnViewmore.setEnabled(false);
                    holder.btnViewmore.setTextColor(activity.getResources().getColor(R.color.veryLightGrey));
                }else {
                    holder.btnViewmore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            activity.getLikeUpdate();
                        }
                    });*/
            //}


            }



        } else {

            Glide.with(activity)
                    .load(Config.IMAGE_BASE_URL + messageDetail.getProfilePicture())
                    .error(R.drawable.avatar)
                    .into(holder.circleImageView);
            holder.txtFirstName.setText(messageDetail.getFirstName() + " " + messageDetail.getLastName());
            holder.txtPost.setText(messageDetail.getMessage());
            holder.txtDateTime.setText(Global.formatedDate(messageDetail.getEstTime()));
            holder.txtNum.setText(messageDetails.get(position).getLikeCount());


            holder.imgLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.imgLike.isChecked()) {
                        holder.imgDislike.setChecked(false);
                    }
                    activity.callReReplyLikeUpdate(1, messageDetails.get(position).getCommentId());
                }
            });

            holder.imgDislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.imgDislike.isChecked()) {
                        holder.imgLike.setChecked(false);
                    }
                    activity.callService(2, messageDetails.get(position).getCommentId());
                }
            });

            if (messageDetails.get(position).getUseroption().equals("0")) {
                holder.imgLike.setChecked(false);
                holder.imgDislike.setChecked(false);
            } else if (messageDetails.get(position).getUseroption().equals("1")) {
                holder.imgLike.setChecked(true);
            } else if (messageDetails.get(position).getUseroption().equals("2")) {
                holder.imgDislike.setChecked(true);
            }

            String country = messageDetail.getMessage().toLowerCase(Locale.getDefault());
            if (country.contains(search)) {
                Log.e("test", country + " contains: " + search);
                int startPos = country.indexOf(search);
                int endPos = startPos + search.length();

                Spannable spanText = Spannable.Factory.getInstance().newSpannable(holder.txtPost.getText()); // <- EDITED: Use the original string, as `country` has been converted to lowercase.
                spanText.setSpan(new ForegroundColorSpan(Color.BLUE), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                holder.txtPost.setText(spanText, TextView.BufferType.SPANNABLE);
            }

        }


    }

    @Override
    public int getItemCount() {
        return messageDetails.size();
    }

    @Override
    public int getItemViewType(int position) {
        return messageDetails.get(position).getViewType();
    }

    @OnClick(R.id.imgDislike)
    public void onViewDislikeClicked() {
        dashboardCallback.dashboardCallback(2);
    }

    @OnClick(R.id.imgLike)
    public void onViewLikeClicked() {
        dashboardCallback.dashboardCallback(1);
    }

    public void getSearchValues(String str) {
        search = str;
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(R.id.circleImageView)
        CircleImageView circleImageView;

        @BindView(R.id.txtPost)
        AppCompatTextView txtPost;
        @BindView(R.id.txtDateTime)
        AppCompatTextView txtDateTime;
        @BindView(R.id.txtNum)
        AppCompatTextView txtNum;
        @BindView(R.id.layoutLike)
        LinearLayout layoutLike;
        @BindView(R.id.imgLike)
        AppCompatCheckBox imgLike;
        @BindView(R.id.imgDislike)
        AppCompatCheckBox imgDislike;
        @Nullable
        @BindView(R.id.txtAnswers)
        AppCompatTextView txtAnswers;
        @BindView(R.id.btnReply)
        AppCompatButton btnReply;
        @BindView(R.id.parking_layout)
        LinearLayout parkingLayout;
        @BindView(R.id.txtFirstName)
        AppCompatTextView txtFirstName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
