package com.agilehealth.ui.dashboard_fragment.model;

/**
 * Created by priya on 22/9/17.
 */

public class ReplyLikeUpdate {
    private String commentId;
    private String type;
    private Long userId;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
