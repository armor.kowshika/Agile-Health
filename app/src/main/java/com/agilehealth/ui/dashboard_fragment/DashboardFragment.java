package com.agilehealth.ui.dashboard_fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agilehealth.R;
import com.agilehealth.app.App;
import com.agilehealth.data.global.Global;
import com.agilehealth.data.util.DrawableClickListener;
import com.agilehealth.data.util.ProgressUtils;
import com.agilehealth.data.util.TitleChangeListener;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.session.Profile;
import com.agilehealth.ui.dashboard_fragment.adapter.DashboardAdapter;
import com.agilehealth.ui.dashboard_fragment.adapter.DashboardCallback;
import com.agilehealth.ui.dashboard_fragment.adapter.DashboardParkingLotAdapter;
import com.agilehealth.ui.dashboard_fragment.adapter.SimpleDividerItemDecoration;
import com.agilehealth.ui.dashboard_fragment.model.CommentLikeUpdate;
import com.agilehealth.ui.dashboard_fragment.model.Dashboardmodel;
import com.agilehealth.ui.dashboard_fragment.model.MostPopular;
import com.agilehealth.ui.dashboard_fragment.model.MostPopularResponse;
import com.agilehealth.ui.dashboard_fragment.model.PostRequest;
import com.agilehealth.ui.dashboard_fragment.model.ReplyLikeUpdate;
import com.agilehealth.ui.dashboard_fragment.presenter.DashboardPresenter;
import com.agilehealth.ui.dashboard_fragment.presenter.DashboardView;
import com.agilehealth.util.toast.ToastMaker;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DashboardFragment extends Fragment implements DashboardView, DatePickerDialog.OnDateSetListener {

    private final String TAG = this.getClass().getSimpleName();
    public static final String RESPONSE="PARKINGLOT_RESPONSE";
    @Inject
    DashboardPresenter presenter;

    Unbinder unbinder;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.chk_expand)
    AppCompatCheckBox chkExpand;
    @BindView(R.id.textview)
    TextView textview;
    @BindView(R.id.textview1)
    TextView textview1;
    @BindView(R.id.chk_arrow)
    AppCompatCheckBox chkArrow;
    @BindView(R.id.parking_layout)
    RelativeLayout parkingLayout;
    @BindView(R.id.img_calender)
    ImageView imgCalender;
    @BindView(R.id.txt_calender)
    TextView txtCalender;
    @BindView(R.id.img_info)
    ImageView imgInfo;
    @BindView(R.id.calender_layout)
    RelativeLayout calenderLayout;
    @BindView(R.id.editSearch)
    EditText editSearch;
    @BindView(R.id.radio_mostPopular)
    RadioButton radioMostPopular;
    @BindView(R.id.radio_mostRecent)
    RadioButton radioMostRecent;
    @BindView(R.id.radiogroup)
    RadioGroup radiogroup;
    @BindView(R.id.parking_recycerView)
    RecyclerView parkingRecycerView;
    BottomSheetBehavior bottomSheetBehavior;
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    List<Profile> profile;
    String date, msgId, cmdId;
    int like, replyLike;
    @BindView(R.id.editPost)
    EditText editPost;
    @BindView(R.id.btnPost)
    AppCompatButton btnPost;
    @BindView(R.id.postLayout)
    RelativeLayout postLayout;
    @BindView(R.id.btnClear)
    AppCompatButton btnClear;
    @BindView(R.id.btnDone)
    AppCompatButton btnDone;
    @BindView(R.id.layoutClearPost)
    RelativeLayout layoutClearPost;
    ProgressUtils progressUtils;
    DashboardParkingLotAdapter adapter;
    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;
    @BindView(R.id.btnViewmore)
    AppCompatButton btnViewmore;
    @BindView(R.id.layout_search)
    LinearLayout layoutSearch;
    @BindView(R.id.dragView)
    RelativeLayout dragView;
    Handler handler;
    MostPopularResponse mostPopular;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_dashboard_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        DaggerAppComponent.create().inject(this);
        presenter.attach(this);
        progressUtils = new ProgressUtils(getActivity());

        if (txtCalender.getText().toString().isEmpty()) {
            txtCalender.setText(Global.Date(Global.getCurrentDate()));
        }

        String time=Global.getCurrentDate();
        Log.e("ESTtime 12 ", ""+time);
        String estDate=Global.formateESTtime(time)+" 00:00:00"+ " AM";
        Log.e("date String ", ""+ estDate);

        Log.e("date 12 ", ""+ Global.date(estDate));
        Log.e("date  ", ""+ Global.datetoString(Global.date(estDate)));


        Global.date(estDate);


        getMostPopular();
        btnpost();
        countDownStart();

        Log.e("Date", " " + txtCalender.getText().toString());

        List<Profile> profileList = App.getInstance().profileDao.queryBuilder().list();
        if (profileList != null) {
            Log.e(TAG, "profileList " + profileList.size());
        } else {
            Log.e(TAG, "profileList null");
        }

       /* final View v = view.findViewById(R.id.dragView);
        ViewTreeObserver viewTreeObserver = v.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    //viewWidth = v.getWidth();
                    //viewHeight = v.getHeight();

                    recyclerView.setLayoutParams(new SlidingUpPanelLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getActivity().getWindowManager().getDefaultDisplay().getHeight() - v.getHeight()));
                }
            });
        }*/

        Log.e("CurrentDateTime", " " + Global.getCurrentDateTime());
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        TimeZone tz = TimeZone.getTimeZone("EST");
        outputFormat.setTimeZone(tz);
        //String current_date =  outputFormat.format(date);
        String current_date = outputFormat.format(date);

        Log.e("current_date", " Date" + date);
        Log.e("current_date", " calender" + calendar);
        Log.e("current_date", " " + current_date);

        return view;
    }

    private void countDownStart() {
        handler = new Handler();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this,1000);
                String time="00:00:00";
               Log.e("ESTtime 12 ", ""+Global.formateESTtime(time));
                String t1=Global.formateESTtime(time);
                String t2=Global.getCurrentDateTime();
                int t3=Integer.parseInt(t1)-Integer.parseInt(t1);
                int currentTime = Log.e("CurrentTime", " " +t3);

            }
        };

    }

    private void btnpost() {
        editPost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                layoutClearPost.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            TitleChangeListener titleChangeListener = (TitleChangeListener) context;
            titleChangeListener.changeTitle(getString(R.string.dashboard));
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUpToolbar() {
    }

    @Override
    public void showProgress() {
        progressUtils.show();

    }

    @Override
    public void hideProgress() {
        progressUtils.dismiss();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void setAdapter(ArrayList<Dashboardmodel> arralist) {
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(new DashboardAdapter(getActivity(), arralist));
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        parkingRecycerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        parkingRecycerView.setHasFixedSize(true);
    }

    @Override
    public void mostPopularResponse(final MostPopularResponse mostPopularResponse) {
        if (mostPopularResponse.getMessageDetails().size() > 0) {
            parkingRecycerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
            adapter = new DashboardParkingLotAdapter(this, mostPopularResponse, mostPopularResponse);
            adapter.setDashboardCallback(new DashboardCallback() {
                @Override
                public void dashboardCallback(int i) {
                    adapter.refresh(mostPopularResponse);
                }
            });
            parkingRecycerView.setAdapter(adapter);
        }
    }

    @Override
    public void mostRecentResponse(final MostPopularResponse mostPopularResponse) {
        if (mostPopularResponse.getMessageDetails().size() > 0) {
            parkingRecycerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
            adapter = new DashboardParkingLotAdapter(this, mostPopularResponse, mostPopularResponse);
            adapter.setDashboardCallback(new DashboardCallback() {
                @Override
                public void dashboardCallback(int i) {
                    adapter.refresh(mostPopularResponse);
                }
            });
            parkingRecycerView.setAdapter(adapter);
        }
          /*  parkingRecycerView.setAdapter(new DashboardParkingLotAdapter(this, mostPopularResponse, mostPopularResponse));
        }
*/
    }

    @Override
    public void getPostResponse() {
        InputMethodManager inputManager = (InputMethodManager) getActivity().
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
        if (radioMostPopular.isChecked()) {
            getMostPopular();
        } else if (radioMostRecent.isChecked()) {
            getMostRecent();
        }
    }

    @Override
    public void getLikeUpdate() {
        Log.e("Likeupdate", " ");
        if (radioMostPopular.isChecked()) {
            getMostPopular();
        } else if (radioMostRecent.isChecked()) {
            getMostRecent();

        }
    }

    @Override
    public void toExpand(MostPopularResponse args) {
        mostPopular=args;
    }

    @OnClick(R.id.chk_expand)
    public void onViewExpandClicked() {
        if (chkExpand.isChecked()) {
            adapter.collapse();
        }else {
            adapter.expand(mostPopular);
        }
    }

    @OnClick(R.id.chk_arrow)
    public void onViewArrowClicked() {
        if (chkArrow.isChecked()) {

        } else {

        }
    }


    @OnClick(R.id.editSearch)
    public void onViewSearchClicked() {
        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getSearchValues(charSequence.toString().toLowerCase());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editSearch.setOnTouchListener(new DrawableClickListener.RightDrawableClickListener(editSearch){
            @Override
            public boolean onDrawableClick() {
                editSearch.setText("");
                return true;
            }
        });
    }

    @OnClick(R.id.radio_mostPopular)
    public void onViewMostPopularClicked() {
        getMostPopular();
    }

    public void getMostPopular() {
        profile = App.getInstance().profileDao.queryBuilder().list();

        MostPopular mostPopular = new MostPopular();
        mostPopular.setType("1");
        mostPopular.setUserId(String.valueOf(profile.get(0).getId()));
        mostPopular.setSortingType("2");
        mostPopular.setSearchDate(Global.getDate(txtCalender.getText().toString()));
        presenter.loadMostPopular(mostPopular);
    }

    @OnClick(R.id.radio_mostRecent)
    public void onViewMostRecectClicked() {
        getMostRecent();
    }

    public void getMostRecent() {
        profile = App.getInstance().profileDao.queryBuilder().list();

        MostPopular mostPopular = new MostPopular();
        mostPopular.setType("1");
        if (profile.size() > 0) {
            mostPopular.setUserId(String.valueOf(profile.get(0).getId()));
        }

        mostPopular.setSortingType("1");
        mostPopular.setSearchDate(Global.getDate(txtCalender.getText().toString()));

        presenter.loadMostRecent(mostPopular);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        date = Global.getFormattedDate(year, monthOfYear, dayOfMonth);
        txtCalender.setText(date);
        if (radioMostPopular.isChecked()) {
            getMostPopular();
        } else if (radioMostRecent.isChecked()) {
            getMostRecent();
        }

    }

    @OnClick(R.id.img_calender)
    public void onViewCalenderClicked() {
        calendar = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(DashboardFragment.this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setMaxDate(Calendar.getInstance());
        calendar.add(Calendar.MONTH, -1);
        datePickerDialog.setMinDate(calendar);
        datePickerDialog.setAccentColor(ContextCompat.getColor(getActivity(), R.color.menu_grey));
        datePickerDialog.show(getActivity().getFragmentManager(), "DatePickerDialog");
    }


    @OnClick(R.id.img_info)
    public void onViewInfoClicked() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(R.string.iflow_dialog);
        alertDialog.setMessage(R.string.dialog_content);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                alertDialog.create().dismiss();
            }
        });
        alertDialog.show();
    }


    @OnClick({R.id.btnClear, R.id.btnDone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnClear:
                editPost.setText("");
                break;
            case R.id.btnDone:
                InputMethodManager inputManager = (InputMethodManager) getActivity().
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                layoutClearPost.setVisibility(View.GONE);
                break;
        }
    }

    @OnClick(R.id.btnPost)
    public void onViewPostClicked() {
        if (editPost.getText().toString().isEmpty()) {
            ToastMaker.makeToast(getActivity(), "Enter your question here");
        } else {
            profile = App.getInstance().profileDao.queryBuilder().list();

            PostRequest postRequest = new PostRequest();
            postRequest.setQuestionId("0");
            postRequest.setUserId(profile.get(0).getId());
            postRequest.setEstTime(Global.getCurrentDateTime());
            postRequest.setCategoryId("0");
            postRequest.setMessage(editPost.getText().toString());
            presenter.setPost(postRequest);
        }

    }


    public void callService(int i, String messageId) {
        profile = App.getInstance().profileDao.queryBuilder().list();
        like = i;
        msgId = messageId;
        CommentLikeUpdate commentLikeUpdate = new CommentLikeUpdate();
        commentLikeUpdate.setType(String.valueOf(i));
        commentLikeUpdate.setMessageId(msgId);
        commentLikeUpdate.setUserId(profile.get(0).getId());
        Log.e("parkinglot", " ");
        presenter.commentLikeUpdate(commentLikeUpdate);
        Log.e("parkinglot", " callback ");
    }

    public void callReReplyLikeUpdate(int i, String commentId) {
        profile = App.getInstance().profileDao.queryBuilder().list();
        replyLike = i;
        cmdId = commentId;
        ReplyLikeUpdate replyLikeUpdate = new ReplyLikeUpdate();
        replyLikeUpdate.setCommentId(cmdId);
        replyLikeUpdate.setUserId(profile.get(0).getId());
        replyLikeUpdate.setType(String.valueOf(replyLike));
        presenter.replyLikeUpdate(replyLikeUpdate);

    }

    public void callReply() {
        postLayout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btnViewmore)
    public void onViewMoreClicked() {
        if (radioMostRecent.isChecked()){
            getMostRecent();
        }else if (radioMostPopular.isChecked()){
            getMostPopular();
        }
    }

    public void vieMore(String totalcount){
        if (totalcount!=null) {
            if (Integer.parseInt(totalcount) < 20) {
                btnViewmore.setEnabled(false);
                btnViewmore.setTextColor(getResources().getColor(R.color.veryLightGrey));
            } else {
                btnViewmore.setEnabled(true);
            }
        }
    }
}
