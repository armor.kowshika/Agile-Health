package com.agilehealth.ui.dashboard_fragment.model;

import com.agilehealth.ui.login.googlesign.ServerValue;

/**
 * Created by root on 19/9/17.
 */

public class MostPopular {

    String type;
    String sortingType;
    String userId;
    String searchDate;




    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSortingType() {
        return sortingType;
    }

    public void setSortingType(String sortingType) {
        this.sortingType = sortingType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }


}
