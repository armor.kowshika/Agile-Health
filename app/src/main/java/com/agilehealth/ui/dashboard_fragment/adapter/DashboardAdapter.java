package com.agilehealth.ui.dashboard_fragment.adapter;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agilehealth.R;
import com.agilehealth.ui.dashboard_fragment.model.Dashboardmodel;
import com.agilehealth.ui.dashboard_activity.DashboardActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {

    private final FragmentActivity activity;
    private final ArrayList<Dashboardmodel> arraylist;

    public DashboardAdapter(FragmentActivity activity, ArrayList<Dashboardmodel> arraylist) {
        this.activity = activity;
        this.arraylist = arraylist;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_dashboard_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.imgView.setImageResource(arraylist.get(position).getImg());
        holder.txtValues.setText(arraylist.get(position).getItem());

        holder.layoutDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(activity,DashboardActivity.class);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgView)
        ImageView imgView;
        @BindView(R.id.txt_values)
        TextView txtValues;
        @BindView(R.id.txt_percentage)
        TextView txtPercentage;
        @BindView(R.id.layout_dashboard)
        RelativeLayout layoutDashboard;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
