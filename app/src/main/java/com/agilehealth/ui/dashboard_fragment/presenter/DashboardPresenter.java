package com.agilehealth.ui.dashboard_fragment.presenter;

import com.agilehealth.base.Presenter;
import com.agilehealth.ui.dashboard_fragment.model.CommentLikeUpdate;
import com.agilehealth.ui.dashboard_fragment.model.MostPopular;
import com.agilehealth.ui.dashboard_fragment.model.PostRequest;
import com.agilehealth.ui.dashboard_fragment.model.ReplyLikeUpdate;

/**
 * Created by ck-kowshika on 14/9/17.
 */

public interface DashboardPresenter extends Presenter<DashboardView> {

    void loadMostPopular(MostPopular mostPopular);
    void loadMostRecent(MostPopular mostPopular);
    void setPost(PostRequest postRequest);
    void commentLikeUpdate(CommentLikeUpdate commentLikeUpdate);
    void replyLikeUpdate(ReplyLikeUpdate replyLikeUpdate);
}
