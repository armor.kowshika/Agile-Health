package com.agilehealth.ui.dashboard_fragment.model;

/**
 * Created by ck-kowshika on 15/9/17.
 */

public class Dashboardmodel {
    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    String item;
    int img;
}
