package com.agilehealth.ui.dashboard_fragment.presenter;

import android.os.Bundle;
import android.util.Log;

import com.agilehealth.R;
import com.agilehealth.app.App;
import com.agilehealth.base.BasePresenter;
import com.agilehealth.data.api.ApiService;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.injection.module.ApiModule;
import com.agilehealth.ui.dashboard_fragment.DashboardFragment;
import com.agilehealth.ui.dashboard_fragment.model.CommentLikeResponse;
import com.agilehealth.ui.dashboard_fragment.model.CommentLikeUpdate;
import com.agilehealth.ui.dashboard_fragment.model.Dashboardmodel;
import com.agilehealth.ui.dashboard_fragment.model.MostPopular;
import com.agilehealth.ui.dashboard_fragment.model.MostPopularResponse;
import com.agilehealth.ui.dashboard_fragment.model.PostRequest;
import com.agilehealth.ui.dashboard_fragment.model.PostResponse;
import com.agilehealth.ui.dashboard_fragment.model.ReplyLikeResponse;
import com.agilehealth.ui.dashboard_fragment.model.ReplyLikeUpdate;

import java.io.Serializable;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ck-kowshika on 14/9/17.
 */

public class DashboardImpl extends BasePresenter<DashboardView> implements DashboardPresenter {

    @Named(ApiModule.DEFAULT)
    @Inject
    ApiService apiService;

    MostPopularResponse mostPopularResponse;

    @Override
    public void attach(DashboardView dashboardView) {
        super.attach(dashboardView);
        DaggerAppComponent.create().inject(this);
        getView().initRecyclerView();
        adapterImpl();
    }

    private void adapterImpl() {
         String item[]={"How are my Product Owners doing?", "How are my teams doing?","How is my organization doing?" ,
                "How are our Engineering practices?","How am I doing as an Agile Practioners?"};

        int img[]={R.drawable.product_owners,R.drawable.teams,R.drawable.organization,R.drawable.engineer,
                R.drawable.agile_practioners};

        ArrayList<Dashboardmodel> arralist=new ArrayList<>();

        for (int i=0;i<5;i++){
            Dashboardmodel dashboard=new Dashboardmodel();
            dashboard.setItem(item[i]);
            dashboard.setImg(img[i]);
            arralist.add(dashboard);
        }
        getView().setAdapter(arralist);

    }


    @Override
    public void loadMostPopular(MostPopular mostPopular) {
        if (App.getInstance().isOnline()){
            getView().showProgress();
            retrofit2.Call<MostPopularResponse> call=apiService.loadMostPopular(mostPopular);
            call.enqueue(new Callback<MostPopularResponse>() {
                @Override
                public void onResponse(retrofit2.Call<MostPopularResponse> call, Response<MostPopularResponse> response) {
                    if (response.isSuccessful()){
                        getView().hideProgress();
                        mostPopularResponse=response.body();
                        getView().mostPopularResponse(mostPopularResponse);
                        getView().toExpand(mostPopularResponse);
                    }

                }

                @Override
                public void onFailure(retrofit2.Call<MostPopularResponse> call, Throwable t) {
                    getView().hideProgress();
                    Log.e("Failure"," "+t);
                }
            });
        }
    }

    @Override
    public void loadMostRecent(MostPopular mostPopular) {
        if (App.getInstance().isOnline()){
            getView().showProgress();
            retrofit2.Call<MostPopularResponse> call=apiService.loadMostPopular(mostPopular);
            call.enqueue(new Callback<MostPopularResponse>() {
                @Override
                public void onResponse(retrofit2.Call<MostPopularResponse> call, Response<MostPopularResponse> response) {
                    if (response.isSuccessful()){
                        getView().hideProgress();
                         mostPopularResponse=response.body();
                        getView().mostRecentResponse(mostPopularResponse);
                    }

                }

                @Override
                public void onFailure(retrofit2.Call<MostPopularResponse> call, Throwable t) {
                    getView().hideProgress();
                    Log.e("Failure"," "+t);
                }
            });
        }
    }

    @Override
    public void setPost(PostRequest postRequest) {
        if (App.getInstance().isOnline()){
            getView().showProgress();
            Call<PostResponse> call=apiService.setPost(postRequest);
            call.enqueue(new Callback<PostResponse>() {
                @Override
                public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                    if (response.isSuccessful()){
                        getView().hideProgress();
                        getView().getPostResponse();
                    }
                }

                @Override
                public void onFailure(Call<PostResponse> call, Throwable t) {
                    getView().hideProgress();
                }
            });
        }
    }

    @Override
    public void commentLikeUpdate(CommentLikeUpdate commentLikeUpdate) {
    if (App.getInstance().isOnline()){
        getView().showProgress();
        Log.e("LikeUpdate"," before");
        Call<CommentLikeResponse> call=apiService.getLikeUpdate(commentLikeUpdate);
        call.enqueue(new Callback<CommentLikeResponse>() {
            @Override
            public void onResponse(Call<CommentLikeResponse> call, Response<CommentLikeResponse> response) {
                if (response.isSuccessful()){
                    Log.e("LikeUpdate"," Success");
                    getView().hideProgress();
                    getView().getLikeUpdate();
                }
            }

            @Override
            public void onFailure(Call<CommentLikeResponse> call, Throwable t) {
                getView().hideProgress();
                Log.e("Error"," "+t);
            }
        });
    }
    }

    @Override
    public void replyLikeUpdate(ReplyLikeUpdate replyLikeUpdate) {
        if (App.getInstance().isOnline()){
            getView().showProgress();
            Call<ReplyLikeResponse> call=apiService.getreplyLikeUpdate(replyLikeUpdate);
            call.enqueue(new Callback<ReplyLikeResponse>() {
                @Override
                public void onResponse(Call<ReplyLikeResponse> call, Response<ReplyLikeResponse> response) {
                    if (response.isSuccessful()){
                        getView().hideProgress();
                        getView().getLikeUpdate();
                    }
                }

                @Override
                public void onFailure(Call<ReplyLikeResponse> call, Throwable t) {
                    getView().hideProgress();
                }
            });
        }
    }
}
