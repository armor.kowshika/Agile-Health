package com.agilehealth.ui.dashboard_fragment.model;

import java.util.ArrayList;

/**
 * Created by root on 19/9/17.
 */

public class MostPopularResponse {
    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public ArrayList<MessageDetails> getMessageDetails() {
        return messageDetails;
    }

    public void setMessageDetails(ArrayList<MessageDetails> messageDetails) {
        this.messageDetails = messageDetails;
    }

    String Result;
    String Status;
    ArrayList<MessageDetails> messageDetails;


    }




