package com.agilehealth.ui.dashboard_fragment.model;

/**
 * Created by kowsi on 21/9/17.
 */

public class PostResponse {
    public String Result;
    public String Status;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }


    }

