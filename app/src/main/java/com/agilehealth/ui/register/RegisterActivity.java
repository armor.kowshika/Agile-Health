package com.agilehealth.ui.register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.agilehealth.R;
import com.agilehealth.base.BaseActivity;
import com.agilehealth.data.PrefConnect;
import com.agilehealth.data.global.Global;
import com.agilehealth.data.util.ProgressUtils;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.ui.login.LoginActivity;
import com.agilehealth.ui.login.googlesign.SharedPrefManager;
import com.agilehealth.ui.register.model.Register;
import com.agilehealth.ui.register.presenter.RegisterPresenter;
import com.agilehealth.ui.register.presenter.RegisterView;
import com.agilehealth.ui.main.MainActivity;
import com.agilehealth.util.toast.ToastMaker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements RegisterView,GoogleApiClient.OnConnectionFailedListener{


    @Inject
    RegisterPresenter Presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtFirstName)
    AppCompatEditText edtFirstName;
    @BindView(R.id.edtLastName)
    AppCompatEditText edtLastName;
    @BindView(R.id.edtEmail)
    AppCompatEditText edtEmail;
    @BindView(R.id.edtPassword)
    AppCompatEditText edtPassword;
    @BindView(R.id.edtPhone)
    AppCompatEditText edtPhoneNumber;
    @BindView(R.id.edtConfm)
    AppCompatEditText edtConfm;
    @BindView(R.id.chkTerms)
    AppCompatCheckBox chkTerms;
    @BindView(R.id.btnTerms)
    AppCompatTextView btnTerms;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.btnRegister)
    AppCompatButton btnRegister;
    @BindView(R.id.edtCompany)
    AppCompatEditText edtCompany;
    private ProgressUtils progressUtils;
    private String mUsername, mEmail;
    SharedPrefManager sharedPrefManager;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth;
    Context mContext = this;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        ButterKnife.bind(this);
        DaggerAppComponent.create().inject(this);
        Presenter.attach(this);
        progressUtils = new ProgressUtils(this);
        sharedPrefManager = new SharedPrefManager(mContext);
        mUsername = sharedPrefManager.getName();
        mEmail = sharedPrefManager.getUserEmail();
        //Set data gotten from SharedPreference to the Navigation Header view
        edtFirstName.setText(mUsername);
        edtEmail.setText(mEmail);
        configureSignIn();
    }

    @Override
    public void isNetworkAvailable(boolean isAvailable)
    {
        if (!isAvailable)
        {
            ToastMaker.makeToast(this, "No internet connection");
        }
    }

    @Override
    public void setUpToolbar()
    {

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void showProgress() {
   progressUtils.show();
    }

    @Override
    public void hideProgress() {
    progressUtils.dismiss();
    }

    @OnClick(R.id.btnRegister)
    public void onViewClicked(View view)
    {
        switch (view.getId()) {

            case R.id.btnRegister:
                 if(validate())
                 {
                     Register register=new Register();
                     register.setFirstName(edtFirstName.getText().toString());
                     register.setLastName(edtLastName.getText().toString());
                     register.setCompany(edtCompany.getText().toString());
                     register.setEmail(edtEmail.getText().toString());
                     register.setDeviceType("2");
                     register.setDeviceToken(PrefConnect.readString(this, PrefConnect.FCM_TOKEN, " "));
                     Log.e("tag","result "+PrefConnect.readString(this, PrefConnect.FCM_TOKEN, " ") );
                     register.setEnvironment("0");
                     register.setMobile(" ");
                     register.setAgileCertification(" ");
                     register.setTitle(" ");
                     register.setPassword(edtPassword.getText().toString());
                     Presenter.gotoVerify(register,getApplicationContext());
                 }

                break;

        }
    }

    private boolean validate()
    {

        if(edtFirstName.getText().toString().isEmpty())
        {
            Global.Toast(RegisterActivity.this,"Enter First Name");
            return false;
        }
        else if(edtLastName.getText().toString().isEmpty())
        {
            Global.Toast(RegisterActivity.this,"Enter Last Name");
            return false;
        }
        else if(edtCompany.getText().toString().isEmpty())
        {
            Global.Toast(RegisterActivity.this,"Enter Company Name");
            return false;
        }
        else if(edtEmail.getText().toString().isEmpty())
        {
            Global.Toast(RegisterActivity.this,"Enter Email");
            return false;
        }
        else if(edtPassword.getText().toString().isEmpty())
        {
            Global.Toast(RegisterActivity.this,"Enter Password");
            return false;
        }
        else if(edtConfm.getText().toString().isEmpty())
        {
            Global.Toast(RegisterActivity.this,"Enter Confirm Password");
            return false;
        }
        else if(edtPhoneNumber.getText().toString().isEmpty())
        {
            Global.Toast(RegisterActivity.this,"Enter Phone Number");
            return false;

        }
        else if (edtPassword.getText().toString().length() < 6) {
            Global.Toast(RegisterActivity.this, "The password should have minimum 6 characters");
            return false;
        }
        else if(!edtPassword.getText().toString().trim().equalsIgnoreCase(edtConfm.getText().toString()))
        {
            Global.Toast(RegisterActivity.this,"Enter Equal Password");
            return false;
        }
        else if (!edtEmail.getText().toString().isEmpty() && !edtEmail.getText().toString().trim().matches(Global.EMAIL_PATTERN)) {
            Global.Toast(RegisterActivity.this, "Enter valid email id");
            return false;

        }
        else if (!edtEmail.getText().toString().trim().matches(Global.EMAIL_PATTERN)) {
            Global.Toast(RegisterActivity.this, "Enter valid email id");
            return false;

        }
        else if (edtPhoneNumber.getText().toString().length() != 10 || !edtPhoneNumber.getText().toString().trim().matches(Global.MOBILE_PATTERN)) {
            Global.Toast(RegisterActivity.this, "Enter valid number");
            return false;

        }

        else if (!chkTerms.isChecked()) {
            Global.Toast(RegisterActivity.this, "Please agree the terms and conditions");
            return false;
        }
        return true;

    }

    @Override
    public void Sucess()
    {
        PrefConnect.writeBoolean(this,PrefConnect.REGISTER,true);
        Intent intent=new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
        //finish();
    }

    public void configureSignIn()
    {
// Configure sign-in to request the user's basic profile like name and email
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .enableAutoManage(this /* FragmentActivity */,  this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();
        mGoogleApiClient.connect();
    }

    //method to logout
    private void signOut()
    {
        new SharedPrefManager(mContext).clear();
        mAuth.signOut();

        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }
        );
    }

    @Override
    public void onLoginFailed(String error)
    {

        ToastMaker.makeToast(this, error);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
