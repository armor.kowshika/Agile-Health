package com.agilehealth.ui.register.presenter;

import android.content.Context;

import com.agilehealth.base.Presenter;
import com.agilehealth.ui.register.model.Register;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public interface RegisterPresenter extends Presenter<RegisterView> {

    void gotoVerify(Register register, Context applicationContext);
}
