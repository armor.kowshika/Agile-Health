package com.agilehealth.ui.register.presenter;

import android.content.Context;
import android.util.Log;

import com.agilehealth.app.App;
import com.agilehealth.base.BasePresenter;
import com.agilehealth.data.PrefConnect;
import com.agilehealth.data.api.ApiService;
import com.agilehealth.injection.component.DaggerAppComponent;
import com.agilehealth.injection.module.ApiModule;
import com.agilehealth.session.Profile;
import com.agilehealth.ui.register.model.Register;
import com.agilehealth.ui.register.model.RegisterResponse;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public class RegisterImpl extends BasePresenter<RegisterView> implements RegisterPresenter {

    public String TAG = this.getClass().getSimpleName();
    @Named(ApiModule.DEFAULT)
    @Inject
    ApiService apiService;

    String tag,firstname,lastname,email,password;

    Context context;


    @Override
    public void attach(RegisterView registerView) {
        super.attach(registerView);
        DaggerAppComponent.create().inject(this);
        getView().setUpToolbar();
    }

    @Override
    public void gotoVerify(Register register, final Context applicationContext) {
       /* if (App.getInstance().isOnline())
        {*/
        getView().showProgress();
        Call<RegisterResponse> call = apiService.register(register);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                getView().hideProgress();

                if (response.isSuccessful())

                {
                    context = applicationContext;

                    if (response.body().getStatus().equals("Already registered, please login")) {
                        getView().onLoginFailed("Already registered, please login");
                    } else if (response.body().getStatus().equals("Account verification link has been sent to your mail")) {
                        Log.e(TAG, "Response Result " + response.body().getResult());
                        Log.e(TAG, "Response UserDetails " + response.body().getUserdetails());
                        RegisterResponse.UserDetails details = response.body().getUserdetails();
                        Profile profile = new Profile();
                        profile.setId(Long.parseLong(""+details.getId()));
                        profile.setFirstName(details.getFirstName());
                        profile.setLastName(details.getLastName());
                        profile.setEmail(details.getEmail());
                        profile.setMobile(details.getMobile());
                        profile.setProfileStatus(details.getProfileStatus());
                        profile.setAuthToken(details.getAuthToken());
                        profile.setPassword(details.getPassword());
                        profile.setProfilePicture(details.getProfilePicture());
                        Log.e(TAG,"picture" +details.getProfilePicture() );
                        profile.setDeviceType(details.getDeviceType());
                        profile.setTitle(details.getTitle());
                        profile.setAgileCertification(details.getAgileCertification());
                        profile.setCompany(details.getCompany());
                        profile.setIsLogin(details.getIsLogin());
                        App.getInstance().profileDao.insertOrReplace(profile);

                        /*tag=String.valueOf(details.getId());
                        firstname=details.getFirstName();
                        lastname=details.getLastName();
                        email=details.getEmail();
                        password=details.getPassword();
                        PrefConnect.writeString(context,PrefConnect.AUTH,details.getAuthToken());
                        PrefConnect.writeString(context,PrefConnect.LAST_NAME,lastname);
                        PrefConnect.writeString(context,PrefConnect.FIRST_NAME,firstname);
                        PrefConnect.writeString(context,PrefConnect.EMAIL,email);
                        PrefConnect.writeString(context,PrefConnect.ID,tag);
                        PrefConnect.writeString(context,PrefConnect.PASSWORD,password);
                        PrefConnect.writeBoolean(context, PrefConnect.CHECK, true);*/
                        getView().Sucess();
                    }


                }
            }


            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                getView().hideProgress();
                getView().onLoginFailed("Slow internet connection");
                Log.e("failure", t.toString());
            }
        });
    }
        /*else {
            App.getInstance().offline();
        }*/

}
