package com.agilehealth.ui.register.presenter;

import com.agilehealth.base.BaseView;
import com.agilehealth.ui.register.model.UserDetails;

import java.util.List;

/**
 * Created by ck-nivetha on 8/9/17.
 */

public interface RegisterView extends BaseView {

    void Sucess();

    void onLoginFailed(String error);
}
